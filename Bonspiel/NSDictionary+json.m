//
//  NSDictionary+json.m
//  Axonator
//
//  Created by Ashish on 20/01/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import "NSDictionary+json.h"

@implementation NSDictionary (json)
-(NSString *) ToString;
{
   
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:self options:0 error:&err];
    NSString * String = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    //String = [String stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    return  String;
}
@end
