//
//  RulesViewController.h
//  Bonspiel
//
//  Created by Priya on 14/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BonSpiel.h"
@interface RulesViewController : UIViewController
@property (nonatomic,strong) BonSpiel *bonspiel;
@property (weak, nonatomic) IBOutlet UITextView *ruleTextView;
@property (weak, nonatomic) IBOutlet UIImageView *bonspielImageView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;
@end
