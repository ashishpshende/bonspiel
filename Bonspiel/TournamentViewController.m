//
//  TournamentViewController.m
//  Bonspiel
//
//  Created by Priya on 12/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "TournamentViewController.h"
#import "BracketsViewController.h"
#import "BSAPIService.h"
#import "BonSpiel.h"
#import "URLBuilder.h"
#import "RulesViewController.h"
#import "LabelAction.h"
#import "MBProgressHUD/MBProgressHUD.h"
@interface TournamentViewController ()
{
    BonSpiel *bonspiel;
    
}
@end

@implementation TournamentViewController
@synthesize tournament;
@synthesize bonspielImageView;
@synthesize bonspielName;
@synthesize duration;
@synthesize hostingClub;
@synthesize address;
@synthesize phoneNumber;
@synthesize email;
@synthesize contentView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    contentView.hidden=YES;
    // Do any additional setup after loading the view.
    
//    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:146.0/255.0f green:0 /255.0f blue:19/255.0f alpha:1]];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"navBarTitle.png"]];
    self.navigationController.navigationBar.backgroundColor=[UIColor colorWithRed:146.0/255.0f green:0 /255.0f blue:19/255.0f alpha:1];
    
//    [self.bonspielImageView.layer setBorderWidth:2];
//    [self.bonspielImageView.layer setBorderColor:[UIColor lightGrayColor].CGColor];

    NSDictionary *dict = @{
                           @"bonspiel_id":[self.tournament objectForKey:@"id"]
                           };
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.label.text = @"Loading...";
    [hud showAnimated:YES];
    [[BSAPIService sharedAPIService] loadTournamentWithParam:dict success:^(id responseObject)
    {
        bonspiel = [[BonSpiel alloc]initWithDictionary:responseObject];
        NSURL *photoURL =[URLBuilder fileURL:bonspiel.photo] ;
        [[NSUserDefaults standardUserDefaults] setObject:bonspiel.photo forKey:@"photoURL"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSURLRequest* videoImagerequest = [NSURLRequest requestWithURL:photoURL cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0];
        [NSURLConnection sendAsynchronousRequest:videoImagerequest queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * response, NSData * data,
  NSError * error)
        {
            
   if (!error)
   {
       [self.bonspielImageView setImage:[UIImage imageWithData:data]];
       
   } else
   {
       //do nothing
   }
                               }];
        
        [bonspielName setText:bonspiel.name];
        [duration setText:@""];
        [hostingClub setText:bonspiel.host_club];
        [address setText:bonspiel.address];
        [phoneNumber setText:bonspiel.phone];
        [email setText:bonspiel.email];
        
        [hud hideAnimated:YES];
        contentView.hidden=NO;
    } failure:^(id error)
    {
        contentView.hidden=YES;
        [hud hideAnimated:YES];
    } progress:^(double percentage) {
        
    }];
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed: @"Backcolor.png"]
                                                  forBarMetrics:UIBarMetricsDefault];
        UIImageView *navBarImageView =[[UIImageView alloc] initWithImage: [UIImage imageNamed:@"navBarTitle.png"]];
        [navBarImageView setContentMode:UIViewContentModeScaleAspectFit];
        self.navigationItem.titleView =navBarImageView;
    [self setOnTapGesture];
}

-(void) setOnTapGesture{
    address.userInteractionEnabled=YES;
    email.userInteractionEnabled=YES;
    phoneNumber.userInteractionEnabled=YES;
    
    
    UITapGestureRecognizer *tapAddress=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapLabelAddress)];
    [address addGestureRecognizer:tapAddress];

    UITapGestureRecognizer *tapEmail=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapLabelEmail)];
    [email addGestureRecognizer:tapEmail];
    
    UITapGestureRecognizer *tapPhoneNumber=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapLabelPhone)];
    [phoneNumber addGestureRecognizer:tapPhoneNumber];

}

-(void) onTapLabelAddress{
    [LabelAction performAction:ADDRESS actionValue:address.text];
}
-(void) onTapLabelEmail{
    [LabelAction performAction:EMAIL actionValue:email.text];
}
-(void) onTapLabelPhone{
    [LabelAction performAction:PHONE actionValue:phoneNumber.text];
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
     [self.navigationItem.leftBarButtonItem setTintColor:[UIColor whiteColor]];
      [self setNeedsStatusBarAppearanceUpdate];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle) preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showBracketViewController"])
    {
        BracketsViewController *bvc =[segue destinationViewController];
        bvc.bonspiel = bonspiel;
    }else if ([[segue identifier] isEqualToString:@"showRulesViewController"])
    {
        RulesViewController *rvc = [segue destinationViewController];
        rvc.bonspiel = bonspiel;
    }
}
- (IBAction)backButtonTapped:(id)sender
{
    UINavigationController *nvc = (UINavigationController*)self.parentViewController;
    [nvc dismissViewControllerAnimated:YES completion:nil];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"secret_word"];
}
@end
