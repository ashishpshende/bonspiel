//
//  BracketViewControllerNew.m
//  Bonspiel
//
//  Created by 𝓙𝓐𝓨𝓔𝓢𝓗 on 26/02/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "BracketViewControllerNew.h"
#import "BSAPIService.h"
#import "RoundTableViewCell.h"
#import "Constant.h"
#import "UpdateScoreViewController.h"

@interface BracketViewControllerNew ()
{
    Event *selectedEvent;
    __weak IBOutlet UILabel *eventNumberLabel;
    __weak IBOutlet UITabBar *evenTabbar;
    __weak IBOutlet UILabel *roundLabel;
    __weak IBOutlet UILabel *nextRoundLabel;
    __weak IBOutlet UITableView *leftTableView;
    __weak IBOutlet UITableView *rightTableView;
    __weak IBOutlet UIView *containerView;
    NSInteger currentPage;//works as index
}
@end

@implementation BracketViewControllerNew
@synthesize loader;
@synthesize bonspiel;
@synthesize noEventLabel;
@synthesize editMatchView;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareBrackets];
    [self setupUI];
    [self  registerCells];
    [self setupGestures];
    
    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"refreshButton.png"] style:UIBarButtonItemStylePlain target:self action:@selector(refreshButtonTapped:)];
    [self.navigationItem setRightBarButtonItem:refreshButton];
    // Do any additional setup after loading the view.
}
-(void) refreshButtonTapped:(id) sender
{
    
}
-(void) setupUI{
    [self.navigationItem setTitle:bonspiel.name];
    [self.navigationItem.leftBarButtonItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed: @"Backcolor.png"] forBarMetrics:UIBarMetricsDefault];
    // Do any additional setup after loading the view.
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(matchViewClicked:) name:@"matchViewClicked" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recordUpdated:) name:@"recordUpdated" object:nil];

}
-(void) setupGestures{
    UISwipeGestureRecognizer *rightRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeHandle:)];
    rightRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [rightRecognizer setNumberOfTouchesRequired:1];
    
    //add the your gestureRecognizer , where to detect the touch..
    [self.view addGestureRecognizer:rightRecognizer];
    
    UISwipeGestureRecognizer *leftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeHandle:)];
    leftRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [leftRecognizer setNumberOfTouchesRequired:1];
    
    [self.view addGestureRecognizer:leftRecognizer];

}
-(void) registerCells{
     [leftTableView registerClass:[RoundTableViewCell class] forCellReuseIdentifier:@"RoundTableViewCell"];
     [rightTableView registerClass:[RoundTableViewCell class] forCellReuseIdentifier:@"RoundTableViewCell"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) prepareBrackets
{
    NSDictionary *dict = @{
                           @"bonspiel_id":[NSNumber numberWithInteger:self.bonspiel.bonspielid]
                           };
    [self .loader startAnimating];
    [[BSAPIService sharedAPIService] loadBonspielEventsWithParam:dict success:^(id responseObject)
     {
         [self .loader stopAnimating];
         self.bonspiel.game_guarantee = [[responseObject objectForKey:@""]integerValue];
         
         NSMutableArray *events = [NSMutableArray array];
         
         for (NSDictionary *dictionary in [responseObject objectForKey:@"events"])
         {
             Event *event = [[Event alloc]initWithDictionary:dictionary];
             event.bonspiel = self.bonspiel;
             [events addObject:event];
         }
         self.bonspiel.events = (NSArray *) events;
         [self sortEvents];
         [self drawBrackets];
         if (events.count !=0)
         {
             [evenTabbar setSelectedItem:[evenTabbar.items objectAtIndex:0]];
             selectedEvent = [self.bonspiel.events objectAtIndex:0];
             [noEventLabel setHidden:YES];
             [roundLabel setHidden:NO];
             [eventNumberLabel setHidden:NO];
         }
         else
         {
             [noEventLabel setHidden:NO];
             [roundLabel setHidden:YES];
             [eventNumberLabel setHidden:YES];
         }
         
         [leftTableView reloadData];
         [rightTableView reloadData];
     } failure:^(id error)
     {
         [self .loader stopAnimating];
     } progress:^(double percentage) {
         
     }];
}

-(void)recordUpdated:(NSNotification *) notification
{
    
    [[BSAPIService sharedAPIService] updateBonspielEventsWithParam:notification.userInfo success:^(id responseObject)
     {
         [self prepareBrackets];
     } failure:^(id error) {
         
     } progress:^(double percentage) {
         
     }];
    
}
-(void) drawBrackets
{
    [evenTabbar setFrame:CGRectMake(0, self.view.frame.size.height-evenTabbar.frame.size.height, self.view.frame.size.width, evenTabbar.frame.size.height)];
    NSMutableArray *tabs = [NSMutableArray array];
    
    for (Event *event in self.bonspiel.events)
    {
        NSString *title = [NSString stringWithFormat:@"Event %ld",(long)event.number];
        UITabBarItem *tabBarItem = [[UITabBarItem alloc]initWithTitle:title  image:[UIImage imageNamed:@"stone_unselected.png"] tag:[self.bonspiel.events indexOfObject:event ]];
        [tabs addObject:tabBarItem];
    }
    [eventNumberLabel setText:@"Event 1"];// for first time load
    evenTabbar .items = tabs;
}
//Gesture handlers

- (void)rightSwipeHandle:(UISwipeGestureRecognizer*)gestureRecognizer
{
    NSLog(@"rightSwipeHandle");
    if(currentPage==0){
        return;
    }
    currentPage--;
    NSLog(@"Left Scrolled.Page:%ld",(long)currentPage);
    [self reloadBothTables:UITableViewRowAnimationAutomatic];
}

- (void)leftSwipeHandle:(UISwipeGestureRecognizer*)gestureRecognizer
{
    NSLog(@"leftSwipeHandle");
    if(currentPage>=[selectedEvent.bracket.rounds count]-2){
        return;
    }
    currentPage++;
    NSLog(@"Right Scrolled.Page:%ld",(long)currentPage);
    [self reloadBothTables:UITableViewRowAnimationAutomatic];
    
}

-(void) reloadBothTables:(UITableViewRowAnimation)animation{
    [leftTableView beginUpdates];
    [leftTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:animation];
    [leftTableView endUpdates];
    
    [rightTableView beginUpdates];
    [rightTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:animation];
    [rightTableView endUpdates];
}

-(void)matchViewClicked:(NSNotification *) notification
{
    UIStoryboard *storybord=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UpdateScoreViewController *updateScoreViewController=[storybord instantiateViewControllerWithIdentifier:@"UpdateScore"];
    updateScoreViewController.selectedMatch=[notification.userInfo objectForKey:@"match"];
    updateScoreViewController.parentVC = self;
    [self.navigationController presentViewController:updateScoreViewController animated:YES completion:nil];
}
//Delgates
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item // called when a new view is selected by the user (but not programatically)
{
    [eventNumberLabel setText:item.title];
    [item setImage:[UIImage imageNamed:@"stone_selected.png"]];
    selectedEvent = [self.bonspiel.events objectAtIndex:item.tag];
    [leftTableView reloadData];
    [rightTableView reloadData];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *matches=nil;
    if(tableView==leftTableView){
        matches=[[selectedEvent.bracket.rounds objectAtIndex:currentPage] matches];
    }else if(tableView==rightTableView){
        matches=[[selectedEvent.bracket.rounds objectAtIndex:currentPage+1] matches];
    }
    return matches.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RoundTableViewCell *cell;
    if(tableView==leftTableView){
        NSArray *matches=[[selectedEvent.bracket.rounds objectAtIndex:currentPage] matches];
        cell = [tableView dequeueReusableCellWithIdentifier:@"RoundTableViewCell" forIndexPath:indexPath];
        cell.matchView.frame=CGRectMake(cell.matchView.frame.origin.x, cell.contentView.bounds.size.height/2, cell.contentView.bounds.size.width/2+100, HEIGHT_BRACKET_CELL/2);
        Match *match= [matches objectAtIndex:indexPath.row];
        cell.match = match;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        roundLabel.text=[NSString stringWithFormat:@"Round %ld",(currentPage+1)];
    }else if(tableView==rightTableView){
        NSArray *matches=[[selectedEvent.bracket.rounds objectAtIndex:currentPage+1] matches];
        cell = [tableView dequeueReusableCellWithIdentifier:@"RoundTableViewCell" forIndexPath:indexPath];
        cell.matchView.frame=CGRectMake(cell.matchView.frame.origin.x, cell.contentView.bounds.size.height/2, cell.contentView.bounds.size.width/2+100, HEIGHT_BRACKET_CELL/2);
        Match *match= [matches objectAtIndex:indexPath.row];
        cell.match = match;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
         nextRoundLabel.text=[NSString stringWithFormat:@"Round %ld",(currentPage+2)];
    }
 
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSInteger count=[selectedEvent.bracket.rounds count];
    
    if (tableView==leftTableView) {
        //Fixed page approach
        return HEIGHT_BRACKET_CELL/2;
        

    }else if(tableView==rightTableView)
    {
        //Fixed page approach
        return HEIGHT_BRACKET_CELL*2;
    }
    return HEIGHT_BRACKET_CELL;

}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
	{
    if(scrollView==leftTableView)
    {
         [rightTableView setContentOffset:scrollView.contentOffset];
    }else{
        [leftTableView setContentOffset:scrollView.contentOffset];
    }
   
}

//Helpers
-(void) reloadData:(NSNotification *)notification
{
    [self .loader setHidesWhenStopped:YES];
    [self.loader setHidden:NO];
    [self prepareBrackets];
}
-(void) sortEvents
{
    NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc]initWithKey:@"number" ascending:YES];
    NSArray * descriptors = [NSArray arrayWithObject:valueDescriptor];
    NSArray *sortedArray=[self.bonspiel.events sortedArrayUsingDescriptors:descriptors];
    self.bonspiel.events = sortedArray;
}


@end
