//
//  TournamentTableViewCell.h
//  Bonspiel
//
//  Created by Priya on 08/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tournament.h"

@interface TournamentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *tournamentImageView;
@property (weak, nonatomic) IBOutlet UILabel *tournamentTitle;
@property (weak, nonatomic) IBOutlet UILabel *tournamentAddress;
@property (strong,nonatomic) Tournament *tournament;
@end
