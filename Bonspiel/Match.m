//
//  Match.m
//  Bonspiel
//
//  Created by Priya on 12/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "Match.h"
#import "NSDictionary+json.h"
@implementation Match
@synthesize  sheet;
@synthesize  match_id;
@synthesize  loser_position;
@synthesize  winner_round;
@synthesize  team1;
@synthesize  team2;
@synthesize  winner_position;
@synthesize  close;
@synthesize  drawTime;
@synthesize round;
@synthesize isStarted;
-(instancetype) initWithDictionary:(NSDictionary *) dictionary
{
    self = [super init];
    if (self)
    {

        self.current_round=[[dictionary objectForKey:@"current_round"] integerValue];
        if ([[dictionary objectForKey:@"sheet"] isKindOfClass:[NSNull class]])
        {
            self.sheet = 0;
        }
        else
             self.sheet = [[dictionary objectForKey:@"sheet"] integerValue];
        
        self.match_id = [[dictionary objectForKey:@"match_id"] integerValue];
        self.loser_position = [dictionary objectForKey:@"loser_position"];
        self.winner_position = [[dictionary objectForKey:@"winner_position"] integerValue];
        self.winner_round = [[dictionary objectForKey:@"winner_round"] integerValue];
        self.loser_round = [[dictionary objectForKey:@"loser_round"] integerValue];
        
        if (![[dictionary objectForKey:@"close"] boolValue] )
        {
            NSLog(@"Match %ld is not closed",self.match_id);
        }
        self.close = [[dictionary objectForKey:@"close"] boolValue] ;
        self.drawTime = [dictionary objectForKey:@"draw_time"];
 
        
        if ([[dictionary objectForKey:@"team1"] isKindOfClass:[NSDictionary class]])
        {
            self.team1 = [[Team alloc]initWithDictionary:[dictionary objectForKey:@"team1"]];
            
            self.team2 = [[Team alloc]initWithDictionary:[dictionary objectForKey:@"team2"]];
        }
        else
        {
            self.team1 = [dictionary objectForKey:@"team1"];
            self.team2 = [dictionary objectForKey:@"team2"];
        }
        
       
    }
    return self;
}
-(NSDictionary *)toDict
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[NSNumber numberWithInteger:self.current_round] forKey:@"current_round"];
    [dict setObject:[NSNumber numberWithInteger:self.sheet] forKey:@"sheet"];
    [dict setObject:[NSString stringWithFormat:@"%ld",(long)self.match_id] forKey:@"match_id"];
    [dict setObject:self.loser_position forKey:@"loser_position"];
    [dict setObject:[NSString stringWithFormat:@"%ld",(long)self.winner_position] forKey:@"winner_position"];
    [dict setObject:[NSNumber numberWithInteger:self.winner_round] forKey:@"winner_round"];
    [dict setObject:[NSNumber numberWithInteger:self.loser_round] forKey:@"loser_round"];
    [dict setObject:[NSNumber numberWithBool:self.close] forKey:@"close"];
    [dict setObject:self.drawTime forKey:@"draw_time"];
    if([self.team1.skip isEqualToString:@""]){
        [dict setObject:@{} forKey:@"team1"];
    }else{
       [dict setObject:[self.team1 toDict] forKey:@"team1"];
    }

    if([self.team2.skip isEqualToString:@""]){
        [dict setObject:@{} forKey:@"team2"];
    }else{
       [dict setObject:[self.team2 toDict] forKey:@"team2"];
    }
    return dict;

  

}
@end
