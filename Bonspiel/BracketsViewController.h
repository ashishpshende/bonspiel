//
//  BracketsViewController.h
//  Bonspiel
//
//  Created by Priya on 12/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BonSpiel.h"
#import "EditMatchView.h"

typedef NS_ENUM(NSInteger,ScrollDirection){
    LEFT,
    RIGHT
};
@interface BracketsViewController : UIViewController <UITabBarDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UIGestureRecognizerDelegate,UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property (weak, nonatomic) IBOutlet EditMatchView *editMatchView;
@property (nonatomic,strong) BonSpiel *bonspiel;
@property (strong, nonatomic) IBOutlet UILabel *noEventLabel;
@property (nonatomic) CGFloat lastContentOffset;
-(void) recordUpdated:(NSString *)message;
@end
