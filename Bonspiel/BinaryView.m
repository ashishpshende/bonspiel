//
//  BinaryView.m
//  Bonspiel
//
//  Created by Priya on 20/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "BinaryView.h"

@implementation BinaryView
{
    UIView *view1;
    UIView *view2;
    UIView *view3;
    UIView *view4;
}
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        view1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width/2, 2)];
        [view1 setBackgroundColor:[UIColor blackColor]];
        [self addSubview:view1];
        
        view2 = [[UIView alloc]initWithFrame:CGRectMake(0,self.frame.size.height-2, self.frame.size.width/2, 2)];
        [view2 setBackgroundColor:[UIColor blackColor]];
        [self addSubview:view2];
        
        view3 = [[UIView alloc]initWithFrame:CGRectMake(self.frame.size.width/2,0, 2, self.frame.size.height)];
        [view3 setBackgroundColor:[UIColor blackColor]];
        [self addSubview:view3];

        view4 = [[UIView alloc]initWithFrame:CGRectMake(self.frame.size.width/2,self.frame.size.height/2, self.frame.size.width/2, 2)];
        [view4 setBackgroundColor:[UIColor blackColor]];
        [self addSubview:view4];
    
    }
    return self;
}
-(void) updateView
{
    [view1 setFrame:CGRectMake(0, 0, self.frame.size.width/2, 2)];
    [view2 setFrame:CGRectMake(0,self.frame.size.height-2, self.frame.size.width/2, 2)];
    [view3 setFrame:CGRectMake(self.frame.size.width/2,0, 2, self.frame.size.height)];
    [view4 setFrame:CGRectMake(self.frame.size.width/2,self.frame.size.height/2, self.frame.size.width/2, 2)];
}

@end
