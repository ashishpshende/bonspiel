//
//  NSString+Json.m
//  Axonator
//
//  Created by Ashish on 20/01/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import "NSString+Json.h"

@implementation NSString (Json)

-(NSDictionary *) ToDictionary
{

    NSDictionary *dict;
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    @try
    {
         dict = [[NSDictionary alloc]initWithCoder:json];
    }
    @catch (NSException *exception)
    {
        
        return json;
    }
    @finally
    {
        
    }

    return dict;
}

-(NSArray *) ToArray
{

    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSError* err;
    NSArray *array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
    
    if (!array)
    {
        
    }
    return array;
}


@end
