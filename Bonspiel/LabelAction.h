//
//  LabelAction.h
//  Bonspiel
//
//  Created by 𝓙𝓐𝓨𝓔𝓢𝓗 on 09/02/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSInteger,ActionType){
    PHONE=1,
    EMAIL,
    ADDRESS
};
@interface LabelAction : NSObject
+(void) performAction:(ActionType)labelActionType actionValue:(NSString*)labelValue;
@end
