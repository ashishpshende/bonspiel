//
//  RoundTableViewCell.h
//  Bonspiel
//
//  Created by Priya on 12/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Round.h"
#import "MatchView.h"
#import "Match.h"
@interface RoundTableViewCell : UITableViewCell
@property(nonatomic,strong)  MatchView *matchView;
@property  (nonatomic,strong) Match *match;
-(void) setMatch:(Match *)match;
@end
