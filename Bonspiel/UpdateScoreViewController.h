//
//  UpdateScoreViewController.h
//  Bonspiel
//
//  Created by 𝓙𝓐𝓨𝓔𝓢𝓗 on 16/02/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckBoxButton.h"
#import "Match.h"
#import "Event.h"
#import "Round.h"
#define TEAM1_HAMMER 1
#define TEAM2_HAMMER 2

@interface UpdateScoreViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *team1HammerImageView;
@property (weak, nonatomic) IBOutlet UILabel *skip1Label;
@property (weak, nonatomic) IBOutlet UIImageView *team2HammerImageView;
@property (weak, nonatomic) IBOutlet UILabel *skip2Label;
@property (weak, nonatomic) IBOutlet UILabel *score1ValueLabel;
@property (weak, nonatomic) IBOutlet UIStepper *score1Stepper;
@property (weak, nonatomic) IBOutlet UILabel *score2ValueLabel;
@property (weak, nonatomic) IBOutlet UIStepper *score2Stepper;
@property (weak, nonatomic) IBOutlet UILabel *endValueLabel;
@property (weak, nonatomic) IBOutlet UIStepper *endStepper;
@property (weak, nonatomic) IBOutlet UISwitch *markFinalSwitch;

@property (weak, nonatomic) IBOutlet UITextField *secretWordTextField;
@property (strong,nonatomic) Match *selectedMatch;
@property(nonatomic) NSInteger selectedTeam;//for hammer

@property (strong, nonatomic)  UIViewController *parentVC;

- (IBAction)score1StepperValueChangedAction:(id)sender;
- (IBAction)score2StepperValueChangedAction:(id)sender;
- (IBAction)endStepperValueChangedAction:(id)sender;
- (IBAction)finalValueChangedAction:(id)sender;
- (IBAction)cancelScoreUpdateAction:(id)sender;
- (IBAction)updateScoreAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *updateButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@end
