//
//  Switch.m
//  Bonspiel
//
//  Created by Priya on 13/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "Switch.h"

@implementation Switch
{
    BOOL status;
    void (^switchoffvar)(void);
    void (^switchonvar)(void);
}
-(void) setBlocks_switchOn:(void (^)(void))on switchOff:(void (^)(void))off
{
    switchoffvar = off;
    switchonvar = on;
    [self addTarget:self action:@selector(modeChanged) forControlEvents:UIControlEventValueChanged];
}
-(instancetype) initWithFrame:(CGRect)frame switchOn:(void (^)(void))on switchOff:(void (^)(void))off
{
    self = [super initWithFrame:frame];
    if (self)
    {
        switchoffvar = off;
        switchonvar = on;
        [self addTarget:self action:@selector(modeChanged) forControlEvents:UIControlEventValueChanged];
    }
    return self;
}
-(void)modeChanged
{
    if (self.isOn)
        switchonvar();
    else
        switchoffvar();
}
@end
