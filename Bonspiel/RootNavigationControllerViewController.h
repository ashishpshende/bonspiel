//
//  RootNavigationControllerViewController.h
//  Bonspiel
//
//  Created by Priya on 08/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootNavigationControllerViewController : UINavigationController
@property (nonatomic,strong) NSString *keyword;
@end
