//
//  URLBuilder.m
//  Bonspiel
//
//  Created by Priya on 08/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "URLBuilder.h"
#import "Constant.h"
@implementation URLBuilder
+(NSString *)buildURL:(NSString *) apiName
{
    NSString *url = [NSString stringWithFormat:@"%@/%@/",URL,apiName];
    return url;
}
+(NSURL *) fileURL:(NSString *)filePath
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",URL,filePath]];
    return url;
}
@end
