//
//  NetworkService.m
//  Bonspiel
//
//  Created by Priya on 08/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "NetworkService.h"

@interface NetworkService ()

@end

@implementation NetworkService
-(void) authenticate:(NSArray *)userDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage
{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
}
-(void) searchWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage
{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
}
-(void) loadTournamentWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage
{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
}
-(void) loadBonspielEventsWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage
{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
}
-(void) updateBonspielEventsWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage
{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
}
@end
