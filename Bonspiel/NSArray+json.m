//
//  NSArray+json.m
//  Axonator
//
//  Created by Ashish on 20/01/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import "NSArray+json.h"

@implementation NSArray (json)


-(NSString *) objectArrayToString
{

    NSMutableArray *postDict = [[NSMutableArray alloc]initWithArray:self];
    NSData *data = [NSJSONSerialization dataWithJSONObject:postDict options:0 error:nil];
    NSString *String = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
  //  String = [String stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    return String;
}

-(NSString *) StringArrayToString
{

    NSString *string;
    for (NSString *str in self)
    {
        string = [NSString stringWithFormat:@"%@,%@",string,str];
    }
    return string;
}
-(NSString *) dataWithJSONObject
{

    NSMutableString *String =  [[NSMutableString alloc] init];
    for (NSObject * obj in self) 
    {
       // [String appendString:[obj description]];
        //change : added following
        NSData *data = [NSJSONSerialization dataWithJSONObject:obj options:0 error:nil];
        NSString *stringData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        [String appendString:stringData];
    }
    return String;
}
@end
