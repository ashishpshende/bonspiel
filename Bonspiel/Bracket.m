
//
//  Bracket.m
//  Bonspiel
//
//  Created by Priya on 12/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "Bracket.h"
#import "Round.h"
#import "NSArray+json.h"
#import "Event.h"
@implementation Bracket
@synthesize number_of_extras;
@synthesize rounds;
@synthesize event;
@synthesize isStarted;
-(instancetype) initWithDictionary:(NSDictionary *) dictionary
{
    self = [super init];
    if (self)
    {
        self.number_of_extras = [[dictionary objectForKey:@"number_of_extras"] integerValue]; ;
        
        NSMutableArray *roundArray = [NSMutableArray array];
        NSInteger i=1;
        for (NSArray *roundDictArray in [dictionary objectForKey:@"rounds"])
        {
            Round *round = [[Round alloc]initWithDictionaryArray:roundDictArray];
            round.bracket = self;
            Event *ent = self.event;
            round.isStarted = ent.isStarted;
            round.roundIndex=i++;
            [roundArray addObject:round];
        }
        
        self.rounds = (NSArray *) roundArray;
    }
    return self;
}
-(NSDictionary *)toDict
{
    NSMutableArray *roundDictArray = [NSMutableArray array];
    for (Round *round in self.rounds)
    {
        [roundDictArray addObject:[round toArray]];
    }
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[NSNumber numberWithInteger:self.number_of_extras] forKey:@"number_of_extras"];
    [dict setObject:roundDictArray forKey:@"rounds"];
    
    return dict;
}
@end
