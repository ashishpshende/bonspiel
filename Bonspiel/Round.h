//
//  Round.h
//  Bonspiel
//
//  Created by Priya on 12/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Bracket.h"
//@class Bracket;
@interface Round : NSObject
@property (nonatomic) NSInteger roundIndex;
@property (nonatomic,strong) Bracket *bracket;
@property (nonatomic,strong) NSArray *matches;
@property (nonatomic) BOOL isStarted;
-(instancetype) initWithDictionaryArray:(NSArray *) dictionaryArray;
-(NSArray *) toArray;
@end
