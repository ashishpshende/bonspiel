//
//  NSString+Json.h
//  Axonator
//
//  Created by Ashish on 20/01/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Json)
-(NSDictionary *) ToDictionary;
-(NSArray *) ToArray;
@end
