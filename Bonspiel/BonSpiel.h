//
//  BonSpiel.h
//  Bonspiel
//
//  Created by Priya on 12/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BonSpiel : NSObject

@property (weak, nonatomic) NSString  *address;
@property (weak, nonatomic) NSString  *email;
@property (weak, nonatomic) NSString  *host_club;
@property (strong, nonatomic) NSString  *name;
@property (weak, nonatomic) NSString  *phone;
@property (weak, nonatomic) NSString  *photo;
@property (strong, nonatomic) NSString  *rules;
@property (assign,nonatomic) BOOL isStarted;
@property (nonatomic) NSInteger  bonspielid;
@property (nonatomic) NSInteger  number_of_events;
@property (nonatomic) NSInteger  number_of_sheets;
@property (nonatomic) NSInteger  number_of_teams;
@property (nonatomic) NSInteger  game_guarantee;

@property (nonatomic,strong) NSArray *events;
-(instancetype) initWithDictionary:(NSDictionary *) dictionary;
-(NSDictionary *) toDict;
@end
