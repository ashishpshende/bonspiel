//
//  MatchView.h
//  Bonspiel
//
//  Created by Priya on 12/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Match.h"

@interface MatchView : UIView
@property (nonatomic,strong) Match *match;
@property (nonatomic) int type;

-(void) updateUI;
-(void)setFinalMatch;
-(void) setType:(int)type;
@end
