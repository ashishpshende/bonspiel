//
//  Qualifier.h
//  Bonspiel
//
//  Created by Priya on 13/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Match.h"
@interface Qualifier : NSObject
@property (nonatomic,strong) Match *match1;
@property (nonatomic,strong) Match *match2;
@property (nonatomic,strong) Match *resultMatch;
-(void)print;
@end
