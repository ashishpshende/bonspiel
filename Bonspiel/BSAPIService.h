//
//  BSAPIService.h
//  Bonspiel
//
//  Created by Priya on 10/12/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AXConnetionObserver.h"
@interface BSAPIService : NSObject <AXConnetionObserver>
+(instancetype)sharedAPIService;

-(void) authenticate:(NSArray *)userDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage;;
-(void) searchWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage;;
-(void) loadTournamentWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage;
-(void) loadBonspielEventsWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage;
-(void) updateBonspielEventsWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage;

@end
