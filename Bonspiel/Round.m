//
//  Round.m
//  Bonspiel
//
//  Created by Priya on 12/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "Round.h"
#import "Match.h"
@implementation Round
@synthesize roundIndex;
@synthesize matches;
@synthesize bracket;
@synthesize isStarted;
-(instancetype) initWithDictionaryArray:(NSArray *) dictionaryArray
{
    self = [super init];
    if (self)
    {
        NSMutableArray *matchArray = [NSMutableArray array];
        for (NSDictionary *dictionary in dictionaryArray)
        {
            Match *match = [[Match alloc]initWithDictionary:dictionary];
            match.round = self;
           
            [matchArray addObject:match];
        }
        self.matches = (NSArray*) matchArray;
    }   
    return self;
}
-(NSArray *) toArray
{
    NSMutableArray *matchDictArray = [NSMutableArray array];
    for (Match *match in self.matches)
    {
        [matchDictArray addObject:[match toDict]];
    }
    return matchDictArray;
}
@end
