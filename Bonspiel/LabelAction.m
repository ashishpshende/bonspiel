//
//  LabelAction.m
//  Bonspiel
//
//  Created by 𝓙𝓐𝓨𝓔𝓢𝓗 on 09/02/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LabelAction.h"
@implementation LabelAction
+(void) performAction:(ActionType)labelActionType  actionValue:(NSString*)labelValue{
    NSString *url;
    switch (labelActionType) {
        case PHONE:
            url=[NSString stringWithFormat:@"tel:%@",labelValue];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
            break;
        case EMAIL:
            url=[NSString stringWithFormat:@"mailto:%@",labelValue];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
            break;
        case ADDRESS:
            url=[NSString stringWithFormat:@"http://maps.apple.com/?q=%@",labelValue];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
            break;
        default:
            break;
    }
    
}
@end


