//
//  BracketViewControllerNew.h
//  Bonspiel
//
//  Created by 𝓙𝓐𝓨𝓔𝓢𝓗 on 26/02/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditMatchView.h"
#import "BonSpiel.h"
#import "Event.h"
@interface BracketViewControllerNew : UIViewController<UITabBarDelegate>
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;

@property (weak, nonatomic) IBOutlet EditMatchView *editMatchView;
@property (nonatomic,strong) BonSpiel *bonspiel;
@property (strong, nonatomic) IBOutlet UILabel *noEventLabel;

@end
