//
//  NetworkService.h
//  Bonspiel
//
//  Created by Priya on 08/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NetworkService : NSObject
-(void) authenticate:(NSArray *)userDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage;
-(void) searchWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage;;
-(void) loadTournamentWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage;
-(void) loadBonspielEventsWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage;
-(void) updateBonspielEventsWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage;
@end
