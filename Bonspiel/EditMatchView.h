//
//  EditMatchView.h
//  Bonspiel
//
//  Created by Priya on 13/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Match.h"
#import "Switch.h"
@interface EditMatchView : UIView <UITextFieldDelegate>

-(void) setUI;

@property (nonatomic,strong) IBOutlet UILabel *matchIdLabel;

@property (nonatomic,strong) IBOutlet UILabel *team1Label;
@property (nonatomic,strong) IBOutlet Switch *team1Switch;
@property (nonatomic,strong) IBOutlet UILabel *team2Label;
@property (nonatomic,strong) IBOutlet Switch *team2Switch;

@property (nonatomic,strong) IBOutlet UILabel *team1ScoreLabel;
@property (nonatomic,strong) IBOutlet UIButton *team1ScorePlusButton;
- (IBAction)team1ScorePlusButtonTapped:(UIButton *)sender;

@property (nonatomic,strong) IBOutlet UIButton *team1ScoreMinusButton;
- (IBAction)team1ScoreMinusButtonTapped:(UIButton *)sender;

@property (nonatomic,strong) IBOutlet UILabel *team2ScoreLabel;
@property (nonatomic,strong) IBOutlet UIButton *team2ScorePlusButton;
- (IBAction)team2ScorePlusButtonTapped:(UIButton *)sender;
@property (nonatomic,strong) IBOutlet UIButton *team2ScoreMinusButton;
- (IBAction)team2ScoreMinusButtonTapped:(UIButton *)sender;

@property (nonatomic,strong) IBOutlet UILabel *endLabel;
@property (nonatomic,strong) IBOutlet UIButton *endPlusButton;
- (IBAction)endPlusButtonTapped:(UIButton *)sender;
@property (nonatomic,strong) IBOutlet UIButton *endMinusButton;
- (IBAction)endMinusButtonTapped:(UIButton *)sender;
@property (nonatomic,strong) IBOutlet Switch *endSwitch;


@property (nonatomic,strong) IBOutlet UITextField *secretKeyTextField;

@property (nonatomic,strong) IBOutlet UILabel *sheetLabel;
@property (nonatomic,strong) IBOutlet UILabel *drawTimeLabel;
@property (nonatomic,strong) IBOutlet UILabel *loserPositionLabel;

@property (nonatomic,strong) IBOutlet UIImageView *team1hammerImageView;
@property (nonatomic,strong) IBOutlet UIImageView *team2hammerImageView;

- (IBAction)cancelButtonTapped:(UIButton *)sender;
- (IBAction)submitButtonTapped:(UIButton *)sender;
@property (nonatomic,strong) Match *match;
@end
