//
//  NSArray+json.h
//  Axonator
//
//  Created by Ashish on 20/01/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (json)
-(NSString *) objectArrayToString;
-(NSString *) StringArrayToString;
@end
