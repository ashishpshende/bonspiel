//
//  Bracket.h
//  Bonspiel
//
//  Created by Priya on 12/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "Event.h"

@interface Bracket : NSObject
@property (nonatomic) NSInteger number_of_extras;
@property (nonatomic,strong) NSArray *rounds;
@property (nonatomic,strong) id event;
@property (nonatomic) BOOL isStarted;
-(instancetype) initWithDictionary:(NSDictionary *) dictionary;
-(NSDictionary *)toDict;
@end
