//
//  Event.h
//  Bonspiel
//
//  Created by Priya on 12/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Bracket.h"
#import "BonSpiel.h"

@interface Event : NSObject
@property (nonatomic,strong) BonSpiel *bonspiel;
@property (nonatomic,strong) Bracket *bracket;
@property (nonatomic) NSInteger number;
@property (nonatomic,strong) NSString *name;
@property (assign,nonatomic) BOOL isStarted;
-(instancetype) initWithDictionary:(NSDictionary *) dictionary;
-(NSDictionary *)toDict;
@end
