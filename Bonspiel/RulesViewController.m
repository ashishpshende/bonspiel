//
//  RulesViewController.m
//  Bonspiel
//
//  Created by Priya on 14/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "RulesViewController.h"
#import "BracketsViewController.h"
#import "URLBuilder.h"
@interface RulesViewController ()

@end

@implementation RulesViewController
@synthesize bonspiel;
@synthesize bonspielImageView;
@synthesize ruleTextView;
@synthesize loadingView;
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self setPhoto];
    // Do any additional setup after loading the view.
    [self setTitle:bonspiel.name];
    [ruleTextView setText:bonspiel.rules];
//    [bonspielImageView.layer setBorderWidth:2];
//    [bonspielImageView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"launchBracketsFromRules"])
    {
        BracketsViewController *bvc =[segue destinationViewController];
        bvc.bonspiel = self.bonspiel;
    }
}
-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    [ruleTextView setText:@"1.  Rule 1:............. \n\n2.  Rule 2:............. \n\n3.  Rule 3:............. \n\n4.  Rule 4:.............\n \n5.  Rule 5:............. \n6.  Rule 6:............. \n\n7.  Rule 7:............. \n\n8.  Rule 8:............. \n\n9.  Rule 9:............. \n\n10. Rule 10 :.............\n\n11. Rule 11:............. \n\n12. Rule 12:............. \n\n13. Rule 13:............. \n\n14. Rule 14:............. \n\n15. Rule 15:............. \n\n16. Rule 16:............. \n\n17. Rule 17:............. \n\n18. Rule 18:............. \n\n19. Rule 19:............. \n\n20. Rule 20 :.............\n"];
}

-(void) setPhoto
{
    [loadingView startAnimating];
    NSString *photoURLString =[[NSUserDefaults standardUserDefaults] objectForKey:@"photoURL"] ;
    NSURL *photoURL =[URLBuilder fileURL:photoURLString] ;
    NSURLRequest* videoImagerequest = [NSURLRequest requestWithURL:photoURL cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0];
    [NSURLConnection sendAsynchronousRequest:videoImagerequest queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * response, NSData * data,
                                                                                                                    NSError * error)
     {
         
         if (!error)
         {
             [self.bonspielImageView setImage:[UIImage imageWithData:data]];
             [self.loadingView stopAnimating];
         } else
         {
             //do nothing
         }
     }];
}
@end
