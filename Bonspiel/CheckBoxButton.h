//
//  CheckBoxButton.h
//  Bonspiel
//
//  Created by 𝓙𝓐𝓨𝓔𝓢𝓗 on 16/02/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckBoxButton : UIButton
@property(nonatomic,assign)IBInspectable BOOL isChecked;
@end
