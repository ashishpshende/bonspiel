//
//  OnlineNetworkService.m
//  Bonspiel
//
//  Created by Priya on 08/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "OnlineNetworkService.h"
#import "AFNetworking.h"
#import "URLBuilder.h"
@implementation OnlineNetworkService
{
    AFHTTPRequestOperationManager *OperationManager;
}

-(instancetype) init
{
    self = [super init];
    if (self)
    {
         OperationManager  = [[AFHTTPRequestOperationManager alloc]init];
         OperationManager.responseSerializer = [AFJSONResponseSerializer serializer];
         [OperationManager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
         [OperationManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
         OperationManager.responseSerializer.acceptableContentTypes = [OperationManager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    }
    return self;
}
-(void) authenticate:(NSArray *)userDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)(double))percentage
{
    
    
    AFHTTPRequestOperation *requestOperation = [OperationManager  POST:[URLBuilder buildURL:@"syncdata"] parameters:@{} success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject)
    {
        
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error)
    {
        
    }];
    [requestOperation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
     {
         
     }];
    [requestOperation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead)
     {
         
     }];
    
}
-(void) searchWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)(double))percentage
{
     AFHTTPRequestOperation *requestOperation = [OperationManager  POST:[URLBuilder buildURL:@"searchBonspiel"] parameters:paramDict success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject)
                                                {
                                                    if ([[responseObject objectForKey:@"status"] isEqualToString:@"success"])
                                                    {
                                                         success(responseObject);
                                                    }
                                                    else
                                                    {
                                                        failure([NSString stringWithFormat:@"reason : %@",[responseObject objectForKey:@"reason"]]);
                                                    }
                                                   
                                                } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error)
                                                {
                                                    NSLog(@"%@",error);
                                                        failure(error.localizedDescription);
                                                }];
    [requestOperation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
     {
    
     }];
    
    [requestOperation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead)
     {
         
         
     }];
}
-(void) loadTournamentWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage;
{
 
    AFHTTPRequestOperation *requestOperation = [OperationManager  POST:[URLBuilder buildURL:@"getBonspielInfo"] parameters:paramDict success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject)
                                                {
                                                    
                                                    if ([[responseObject objectForKey:@"status"] isEqualToString:@"success"])
                                                    {
                                                        success(responseObject);
                                                    }
                                                    else
                                                    {
                                                        failure([NSString stringWithFormat:@"reason : %@",[responseObject objectForKey:@"reason"]]);
                                                    }
                                                    
                                                } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error)
                                                {
                                                    NSLog(@"%@",error);
                                                     failure(error.localizedDescription);
                                                }];
    [requestOperation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
     {
         
     }];
    
    [requestOperation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead)
     {
     }];

}
-(void) loadBonspielEventsWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage
{
    OperationManager  = [[AFHTTPRequestOperationManager alloc]init];
    OperationManager.responseSerializer = [AFJSONResponseSerializer serializer];
    [OperationManager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [OperationManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    OperationManager.responseSerializer.acceptableContentTypes = [OperationManager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    AFHTTPRequestOperation *requestOperation = [OperationManager  POST:[URLBuilder buildURL:@"getBonspielEvents"] parameters:paramDict success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject)
                                                {
                                                    
                                                    if ([[responseObject objectForKey:@"status"] isEqualToString:@"success"])
                                                    {
                                                        success(responseObject);
                                                    }
                                                    else
                                                    {
                                                        failure([NSString stringWithFormat:@"reason : %@",[responseObject objectForKey:@"reason"]]);
                                                    }
                                                    
                                                } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error)
                                                {
                                                    NSLog(@"%@",error);
                                                    failure(error.localizedDescription);
                                                }];
    [requestOperation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
     {
         
     }];
    
    [requestOperation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead)
     {
     }];
}
-(void) updateBonspielEventsWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage
{
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [OperationManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    OperationManager.requestSerializer = requestSerializer;
    AFHTTPRequestOperation *requestOperation = [OperationManager  POST:[URLBuilder buildURL:@"postEvent"] parameters:paramDict success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject)
                                                {
                                                    if ([[responseObject objectForKey:@"status"] isEqualToString:@"success"])
                                                    {
                                                        success(responseObject);
                                                    }
                                                    else
                                                    {
                                                        if ([[responseObject objectForKey:@"reason"] isEqualToString:@"wrong_secret_word"])
                                                        {
                                                             failure(@"Wrong Secret Word. Please Retry with correct secret word.");
                                                        }
                                                        else if ([[responseObject objectForKey:@"reason"] isEqualToString:@""])
                                                        {
                                                            failure([responseObject objectForKey:@"reason"]);
                                                        }
                                                    } 
                                                } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error)
                                                {
                                                    failure( [NSString stringWithFormat:@"Unable to update due to %@",error.localizedDescription]);
                                                }];
    [requestOperation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
     {
         
     }];
    
    [requestOperation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead)
     {
     }];
    
}
@end
