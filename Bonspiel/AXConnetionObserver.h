//
//  AXConnetionObserver.h
//  Axonator
//
//  Created by Ashish on 10/01/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AXConnetionObserver <NSObject>

-(void) notifyOnline;
-(void) notifyOffline;

@end
