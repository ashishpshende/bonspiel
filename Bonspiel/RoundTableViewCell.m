//
//  RoundTableViewCell.m
//  Bonspiel
//
//  Created by Priya on 12/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "RoundTableViewCell.h"
#import "BinaryView.h"
#include "Constant.h"
@implementation RoundTableViewCell
{

}
@synthesize matchView;
-(instancetype) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
//        CGRectMake(cell.matchView.frame.origin.x, cell.frame.size.height-220, cell.matchView.frame.size.width, HEIGHT_BRACKET_CELL);
      
        
        //self.matchView = [[MatchView alloc]initWithFrame:CGRectMake(5, self.contentView.bounds.size.height-220, self.contentView.frame.size.width-100, HEIGHT_BRACKET_CELL)];
        
        self.matchView = [[MatchView alloc]initWithFrame:CGRectMake(0, 0, self.contentView.frame.size.width, HEIGHT_BRACKET_CELL/2)];
        
        [self.contentView addSubview:matchView];
        
//        UIView* connectorsView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, HEIGHT_BRACKET_CELL/2)];
//        connectorsView.backgroundColor = [UIColor greenColor];
//        self.accessoryView = connectorsView;
//        
//        bView= [[BinaryView alloc]initWithFrame:CGRectMake(match1View.frame.size.width, match1View.frame.size.height/2, resultMatchView.frame.size.width, match2View.frame.size.height)];
//        [self.contentView addSubview:bView];
    }
    return self;
}
- (void)awakeFromNib
{
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//- (void) layoutSubviews
//{
//    [super layoutSubviews];
//    
//    CGRect contentViewFrame = self.contentView.frame;
//    contentViewFrame.size.width = 302;
//    self.contentView.frame = contentViewFrame;
//    
//}


-(void) setMatch:(Match *)match{
    _match=match;
//    [matchView setFrame:CGRectMake(5, 5, self.contentView.frame.size.width -100, self.frame.size.height/2-10)];
    [matchView setMatch:_match];
}
@end
