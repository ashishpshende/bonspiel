    //
//  ViewController.m
//  Bonspiel
//
//  Created by Priya on 10/12/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import "ViewController.h"
#import "SearchResultsViewController.h"
#import "TournamentViewController.h"
#import "BSAPIService.h"
#import "BonSpiel.h"
#import "URLBuilder.h"
//#import "TournamentViewController.h"
@interface ViewController ()
{
    NSArray *searchResultArray;
    NSDictionary *selectedTournament;
    IBOutlet UITableView *resultTable;
    CGRect orgRect;
}
@end

@implementation ViewController
@synthesize KBSkipView;
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    

   [self.searchTextField addTarget:self action:@selector(textinput_started) forControlEvents:UIControlEventEditingChanged];
//[[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:146.0/255.0f green:0 /255.0f blue:19/255.0f alpha:1]];
    
    // Do any additional setup after loading the view, typically from a nib.
//    orgRect = CGRectMake(70, 100, self.view.frame.size.width, self.view.frame.size.height);
//    self.KBSkipView.frame = orgRect;
    orgRect = self.KBSkipView.frame;
    UIImageView *searchView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.searchTextField.frame.size.height, self.searchTextField.frame.size.height)];
    [searchView setImage:[UIImage imageNamed:@"search.png"]];
    [self.searchTextField setLeftViewMode:UITextFieldViewModeAlways];
    [self.searchTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
    self.searchTextField.leftView = searchView;
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.searchTextField setText:@""];
    [resultTable setHidden:YES];
    [self.searchTextField becomeFirstResponder];
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    [self.KBSkipView setFrame:orgRect];
}
-(void)textinput_started
{
    if (self.searchTextField.text.length >=1)
    {
        [self search];
    }
    else
    {
        [resultTable setHidden:YES];
    }
}
-(void)search
{
    NSDictionary *dict = @{
                           @"bonspiel_chars":self.searchTextField.text
                           };
    [[BSAPIService sharedAPIService] searchWithParam:dict success:^(id responseObject)
     {
         searchResultArray = [responseObject objectForKey:@"bonspiels"];
         //         [self performSegueWithIdentifier:@"pushNavigationVC" sender:self];
         if (searchResultArray.count >=1)
         {
             [resultTable setHidden:NO];
             [resultTable reloadData];
//             [self.searchTextField resignFirstResponder];
         }
         else
         {
             [resultTable setHidden:YES];
         }
     } failure:^(id error)
     {
         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:error delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
         [alert show];
     } progress:^(double percentage) {
         
     }];

}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
   // [self.KBSkipView setFrame:CGRectMake(orgRect.origin.x, orgRect.origin.y-200, orgRect.size.width, orgRect.size.height)];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
//    [textField resignFirstResponder];
//    NSDictionary *dict = @{
//                           @"bonspiel_chars":textField.text
//                           };
//    [[BSAPIService sharedAPIService] searchWithParam:dict success:^(id responseObject)
//     {
//         searchResultArray = [responseObject objectForKey:@"bonspiels"];
////         [self performSegueWithIdentifier:@"pushNavigationVC" sender:self];
//         if (searchResultArray.count >=1)
//         {
//             [resultTable setHidden:NO];
//             [resultTable reloadData];
//         }
//         else
//         {
//             [resultTable setHidden:YES];
//         }
//     } failure:^(id error)
//     {
//         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:error delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//         [alert show];
//     } progress:^(double percentage) {
//         
//     }];

    [textField resignFirstResponder];
     return NO;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return searchResultArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"resultCell" forIndexPath:indexPath];
    NSDictionary *dict = [searchResultArray objectAtIndex:indexPath.row];
    [cell.textLabel setText:[dict objectForKey:@"name"]];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    //    TournamentViewController *TVC = ;
    selectedTournament = [searchResultArray objectAtIndex:indexPath.row];

    NSDictionary *dict = @{
                           @"bonspiel_id":[selectedTournament objectForKey:@"id"]
                           };
    
    [[BSAPIService sharedAPIService] loadTournamentWithParam:dict success:^(id responseObject)
     {
         BonSpiel    *bonspiel = [[BonSpiel alloc]initWithDictionary:responseObject];
         NSURL *photoURL =[URLBuilder fileURL:bonspiel.photo] ;
         NSURLRequest* videoImagerequest = [NSURLRequest requestWithURL:photoURL cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0];
         [NSURLConnection sendAsynchronousRequest:videoImagerequest queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * response, NSData * data,
                                                                                                                           NSError * error)
          {
             
          }];
         [self performSegueWithIdentifier:@"pushNavigationVC" sender:self];
     }
     failure:^(id error)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:[NSString stringWithFormat:@"Unable to get Information due to %@",error] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
     } progress:^(double percentage)
    {
         
     }];
}
     

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
//    if ([[segue identifier] isEqualToString:@"pushNavigationVC"])
//    {
//        UINavigationController *nvc =[segue destinationViewController];
//        SearchResultsViewController *srVC = [nvc.viewControllers objectAtIndex:0];
//        srVC.searchResultArray = searchResultArray;
//    }
    if ([[segue identifier] isEqualToString:@"pushNavigationVC"])
    {
        UINavigationController *nvc =[segue destinationViewController];
        TournamentViewController *tvc =[nvc.viewControllers objectAtIndex:0];
        tvc.tournament = selectedTournament;
    }
}
@end
