//
//  TournamentViewController.h
//  Bonspiel
//
//  Created by Priya on 12/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TournamentViewController : UIViewController
@property (nonatomic,strong) NSDictionary *tournament;
@property (weak, nonatomic) IBOutlet UIImageView *bonspielImageView;
@property (weak, nonatomic) IBOutlet UILabel *bonspielName;
@property (weak, nonatomic) IBOutlet UILabel *duration;
@property (weak, nonatomic) IBOutlet UILabel *hostingClub;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *email;
@property (weak, nonatomic) IBOutlet UIButton *rules;
@property (weak, nonatomic) IBOutlet UIView *contentView;


@end
