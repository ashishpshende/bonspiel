//
//  CustomCollectionViewFlowLayout.h
//  Bonspiel
//
//  Created by 𝓙𝓐𝓨𝓔𝓢𝓗 on 12/02/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCollectionViewFlowLayout : UICollectionViewFlowLayout

@end
