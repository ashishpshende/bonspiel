//
//  MatchView.m
//  Bonspiel
//
//  Created by Priya on 12/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "MatchView.h"
#import "connectorView.h"

#define DETAILS_LABEL_FONT_SIZE 14
@implementation MatchView
{
    //Top Header View
    UIView *HeaderView;
    UILabel *matchIdLabel;
    UILabel *scoreLabel;
    UILabel *endLabel;
    
    //Team View
    UIView *TeamView;
    
    UILabel *team1Label;
    UIImageView *team1HammerImageView;
    UILabel *team1ScoreLabel;
    
    UILabel *team2Label;
    UIImageView *team2HammerImageView;
    UILabel *team2ScoreLabel;
    
    UILabel *endScoreLabel;
    
    //Deatils View
    UIView *DeatilsView;
    UILabel *sheetLabel;
    UILabel *drawTimeLabel;
    UILabel *loserMatchIdLabel;

    CGFloat width;
    CGFloat height;
    UITapGestureRecognizer *tapGesture;
    BOOL FINAL;
    connectorView* lineView;
    UIView* borderView;
}

-(void) setType:(int)type
{
    _type = type;
    lineView.type = _type;
}
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {

        //matchIdLabel///////scoreLabel////////endLabel//
        //////team1Label///team1ScoreLabel///              //
        //////team2Label///team2ScoreLabel///              //
        //sheetLabel////drawTimeLabel//////loserMatchIdLabel//
        
        width =  self.frame.size.width;
        height =  self.frame.size.height;
        
        borderView =  [[UIView alloc]initWithFrame:CGRectMake(0, 5, width-70, height-10)];
        borderView.layer.borderWidth =1 ;
        borderView.layer.borderColor = [[UIColor alloc] initWithRed:178/255.0f green:177/255.0f blue:177/255.0f alpha:1].CGColor;
        [self addSubview:borderView];

        lineView =  [[connectorView alloc]initWithFrame:CGRectMake(width-16-5, 0, 16+5, height)];
        lineView.backgroundColor = [UIColor clearColor];
        lineView.type = self.type;
        [self addSubview:lineView];

        width =  borderView.frame.size.width;
        height =  borderView.frame.size.height;
        
        
        
        UIColor *bgColor=[[UIColor alloc] initWithRed:243/255.0f green:243/255.0f blue:243.0f alpha:1];
        borderView.backgroundColor=bgColor;
        //self.layer.borderWidth=1.0f;
//        self.layer.borderColor=[[UIColor alloc] initWithRed:178/255.0f green:177/255.0f blue:177/255.0f alpha:1];//(__bridge CGColorRef _Nullable)([[UIColor alloc] initWithRed:178/255.0f green:177/255.0f blue:177/255.0f alpha:1]);
        
        UIColor *labelColor=[[UIColor alloc] initWithRed:89/255.0f green:89/255.0f blue:69/255.0f alpha:1];
        
        HeaderView = [[UIView alloc]initWithFrame:CGRectMake(2, 2, width-4, height/5)];
        [borderView addSubview:HeaderView];
       
        
        matchIdLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 2,HeaderView.frame.size.width/3 , HeaderView.frame.size.height-4)];
        [matchIdLabel setTextColor:labelColor];
        [HeaderView addSubview:matchIdLabel];
        [matchIdLabel setFont:[UIFont boldSystemFontOfSize:14]];

        
        scoreLabel = [[UILabel alloc]initWithFrame:CGRectMake(matchIdLabel.frame.origin.x+matchIdLabel.frame.size.width/3, 2, HeaderView.frame.size.width/3, HeaderView.frame.size.height-4)];
        scoreLabel.textColor=labelColor;
        [HeaderView addSubview:scoreLabel];
        scoreLabel.textAlignment=NSTextAlignmentCenter;
        [scoreLabel setText:@"Score"];
        
        endLabel = [[UILabel alloc]initWithFrame:CGRectMake(scoreLabel.frame.origin.x+scoreLabel.frame.size.width, 2, HeaderView.frame.size.width/3, HeaderView.frame.size.height-4)];
        endLabel.textColor=labelColor;
        endLabel.textAlignment=NSTextAlignmentRight;
        [HeaderView  addSubview:endLabel];
        [endLabel setText:@"End"];
        
        UIColor *blueColor=[[UIColor alloc] initWithRed:2/255.0f green:73/255.0f blue:154/255.0f alpha:1];
        UIColor *redColor=[[UIColor alloc] initWithRed:156/255.0f green:21/255.0f blue:35/255.0f alpha:1];
        TeamView =  [[UIView alloc]initWithFrame:CGRectMake(2,HeaderView.frame.size.height + 4, width-4, height/2)];
        [borderView addSubview:TeamView];

        team1Label = [[UILabel alloc]initWithFrame:CGRectMake(15, 5, TeamView.frame.size.height/2, TeamView.frame.size.height/2 - 5)];
        [team1Label setFont:[UIFont boldSystemFontOfSize:12]];
        
        [team1Label setTextAlignment:NSTextAlignmentLeft];
        team1Label.textColor=blueColor;
        [TeamView addSubview:team1Label];
        [team1Label setText:@"Team 1"];
        
        team1HammerImageView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 2, 15, 20)];
        [team1HammerImageView setImage:[UIImage imageNamed:@"hammer.png"]];
        [TeamView addSubview:team1HammerImageView];
        
        team2Label = [[UILabel alloc]initWithFrame:CGRectMake(15, TeamView.frame.size.height/2 + 5, TeamView.frame.size.height, TeamView.frame.size.height/2 - 5)];
        [team2Label setTextAlignment:NSTextAlignmentLeft];
        team2Label.textColor=redColor;
        [team2Label setFont:[UIFont boldSystemFontOfSize:12]];
        [TeamView addSubview:team2Label];
        [team2Label setText:@"Team 2"];
        
        team2HammerImageView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 24, 15, 20)];
        [team2HammerImageView setImage:[UIImage imageNamed:@"hammer.png"]];
        [TeamView addSubview:team2HammerImageView];
    
        team1ScoreLabel = [[UILabel alloc]initWithFrame:CGRectMake(team1Label.frame.origin.x+team1Label.frame.size.width+5, 5, TeamView.frame.size.height, TeamView.frame.size.height/2 - 5)];
        [team1ScoreLabel setFont:[UIFont systemFontOfSize:10]];
        [TeamView addSubview:team1ScoreLabel];
        [team1ScoreLabel setText:@"5"];
        
        team2ScoreLabel = [[UILabel alloc]initWithFrame:CGRectMake(team1Label.frame.origin.x+team1Label.frame.size.width+5, team2Label.frame.origin.y, TeamView.frame.size.height, TeamView.frame.size.height/2 - 5)];
        [team2ScoreLabel setFont:[UIFont systemFontOfSize:10]];
        [TeamView addSubview:team2ScoreLabel];
        [team2ScoreLabel setText:@"5"];
        
        endScoreLabel= [[UILabel alloc]initWithFrame:CGRectMake(TeamView.frame.size.width-TeamView.frame.size.width/4,team1ScoreLabel.frame.origin.x+ team1ScoreLabel.frame.size.height+5, TeamView.frame.size.height, TeamView.frame.size.height/2 - 5)];
        [TeamView addSubview:endScoreLabel];
        endScoreLabel.textAlignment=NSTextAlignmentCenter;
        [endScoreLabel setFont:[UIFont systemFontOfSize:10]];
        [endScoreLabel setText:@"5"];
        

        
        DeatilsView =  [[UIView alloc]initWithFrame:CGRectMake(2, height-height/5, width-4, height/5)];
//        DeatilsView.backgroundColor = [UIColor redColor];
        [borderView addSubview:DeatilsView];
        
        sheetLabel = [[UILabel alloc]initWithFrame:CGRectMake(2, 0, DeatilsView.frame.size.width/3, DeatilsView.frame.size.height-4)];
        [sheetLabel setTextAlignment:NSTextAlignmentCenter];
        [DeatilsView addSubview:sheetLabel];
        [sheetLabel setText:@"Sheet1"];
        [sheetLabel setFont:[UIFont systemFontOfSize:DETAILS_LABEL_FONT_SIZE]];
        
        drawTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(DeatilsView.frame.size.width/3, 2, DeatilsView.frame.size.width/3, DeatilsView.frame.size.height-4)];
        [DeatilsView addSubview:drawTimeLabel];
        [drawTimeLabel setText:@"14.87 PM"];
        [drawTimeLabel setTextAlignment:NSTextAlignmentCenter];
        [drawTimeLabel setFont:[UIFont systemFontOfSize:DETAILS_LABEL_FONT_SIZE]];

        
        loserMatchIdLabel = [[UILabel alloc]initWithFrame:CGRectMake(DeatilsView.frame.size.width-DeatilsView.frame.size.width/3, 2, DeatilsView.frame.size.width/3, DeatilsView.frame.size.height-4)];
        [loserMatchIdLabel setTextAlignment:NSTextAlignmentCenter];
        [DeatilsView addSubview:loserMatchIdLabel];
        [loserMatchIdLabel setText:@"872"];
        [loserMatchIdLabel setFont:[UIFont systemFontOfSize:DETAILS_LABEL_FONT_SIZE]];
        
        //self.layer.borderWidth =1 ;
        //self.layer.borderColor = [[UIColor alloc] initWithRed:178/255.0f green:177/255.0f blue:177/255.0f alpha:1].CGColor;
        
        tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(iAmTapped:)];
        [self addGestureRecognizer:tapGesture];
        tapGesture.numberOfTapsRequired = 2;
    }
    return self;
}
-(void)iAmTapped:(UITapGestureRecognizer *)tapGesture
{
    
    if (_match)
    {
        _match.isStarted = [[NSUserDefaults standardUserDefaults] boolForKey:@"is_started"];
        if ([_match.team1.skip isEqualToString:@""] || [_match.team2.skip isEqualToString:@""])
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"BonSpiel Alert" message:@"This Match is not started yet." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        else
        {
            if (_match.isStarted)
            {
                if (_match.close)
                {
                    BOOL closed = YES;
                    for (Match *mtch in _match.round.matches)
                    {
                        if (!mtch.close)
                        {
                            closed = NO;
                        }
                        
                        else
                        {
                            
                        }
                    }
                    if (closed)
                    {
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"BonSpiel Alert" message:@"All Macthes of this round are closed." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                        [alert show];
                    }
                    else
                    {
                        NSDictionary *dict =@{@"match": _match};
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"matchViewClicked" object:self userInfo:dict];
                    }
                }
                else
                {
                    NSDictionary *dict =@{@"match": _match};
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"matchViewClicked" object:self userInfo:dict];
                    
                }
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"BonSpiel Alert" message:@"BonSpiel is not started yet." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                
            }
        }
       
        
    }
    else
    {
        if (FINAL)
        {
        
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"BonSpiel Alert" message:@"No Match assigned yet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        }
    }
    
}

-(void) updateHeaderView
{
    [HeaderView setFrame:CGRectMake(2, 2, width-4, height/5)];

    [matchIdLabel setFrame:CGRectMake(0, 2,HeaderView.frame.size.width/3 , HeaderView.frame.size.height-4)];
    [scoreLabel setFrame:CGRectMake(matchIdLabel.frame.origin.x+matchIdLabel.frame.size.width/3+75, 2, HeaderView.frame.size.width/3, HeaderView.frame.size.height-4)];
    [endLabel setFrame:CGRectMake(scoreLabel.frame.origin.x+scoreLabel.frame.size.width-5, 2, 55, HeaderView.frame.size.height-4)];
    [matchIdLabel setText:[NSString stringWithFormat:@"%ld",(long)_match.match_id]];
}
-(void) updateTeamView
{
    [TeamView setFrame:CGRectMake(2,HeaderView.frame.size.height + 4, width-4, height/2)];
    [team1Label setFrame:CGRectMake(55, 5, TeamView.frame.size.width/3, TeamView.frame.size.height/2 - 5)];
    [team2Label setFrame:CGRectMake(55, TeamView.frame.size.height/2 + 5, TeamView.frame.size.width/3, TeamView.frame.size.height/2 - 5)];
    [team1ScoreLabel setFrame:CGRectMake(team1Label.frame.origin.x +team1Label.frame.size.width+5, 5, TeamView.frame.size.height, TeamView.frame.size.height/2 - 5)];
    [team2ScoreLabel setFrame:CGRectMake(team2Label.frame.origin.x + team2Label.frame.size.width+5, team2Label.frame.origin.y, TeamView.frame.size.height, TeamView.frame.size.height/2 - 5)];
    [endScoreLabel setFrame:CGRectMake(TeamView.frame.size.width-TeamView.frame.size.width/4,team1ScoreLabel.frame.size.height, TeamView.frame.size.height, TeamView.frame.size.height/2 - 5)];
    
    [team1Label setText:_match.team1.skip];
    [team2Label setText:_match.team2.skip];
    [team1ScoreLabel setText:[NSString stringWithFormat:@"%ld",(long)_match.team1.score]];
    [team2ScoreLabel setText:[NSString stringWithFormat:@"%ld",(long)_match.team2.score]];

    if (![_match.team1.end isEqualToString:@""])
    {
        [endScoreLabel setText:_match.team1.end];
        [team1HammerImageView setHidden:NO];
        [team2HammerImageView setHidden:YES];
    }
    else if(![_match.team2.end isEqualToString:@""])
    {
        [endScoreLabel setText:_match.team2.end];
        [team1HammerImageView setHidden:YES];
        [team2HammerImageView setHidden:NO];
    }
    
    if([_match.team1.end isEqualToString:@""] && [_match.team2.end isEqualToString:@""])
    {
        [team1HammerImageView setHidden:YES];
        [team2HammerImageView setHidden:YES];
        [endScoreLabel setText:@"0"];
    }
    
//    if (_match.team2.end)
//    {
//        [team1HammerImageView setHidden:YES];
//        [team2HammerImageView setHidden:NO];
//
//        [endScoreLabel setText:_match.team2.end];
//    }
//    else
//    {
//        [endScoreLabel setText:_match.team1.end];
////        [team1HammerImageView setHidden:NO];
////        [team2HammerImageView setHidden:YES];
//
//    }
//    if (![_match.team1.end isEqualToString:@""]){
//        [endScoreLabel setText:_match.team1.end];
//    }else if(![_match.team2.end isEqualToString:@""]){
//        [endScoreLabel setText:_match.team2.end];
//    }

}
-(void) updateDetailsView
{
     [DeatilsView setFrame:CGRectMake(2,height -height/5, width-4, height/5)];
    [sheetLabel setFrame:CGRectMake(5, 2, DeatilsView.frame.size.width/3, DeatilsView.frame.size.height-4)];
    [drawTimeLabel setFrame:CGRectMake(DeatilsView.frame.size.width/3, 2, DeatilsView.frame.size.width/3+20, DeatilsView.frame.size.height-4)];
    [loserMatchIdLabel setFrame:CGRectMake(DeatilsView.frame.size.width-DeatilsView.frame.size.width/3, 2, DeatilsView.frame.size.width/3, DeatilsView.frame.size.height-4)];
    [sheetLabel setText:[NSString stringWithFormat:@"Sheet %ld",(long)(_match.sheet+1)]];
    [drawTimeLabel setText:_match.drawTime];
    [loserMatchIdLabel setText:_match.loser_position];
}
-(void) updateUI
{
    width =  borderView.frame.size.width;
    height =   borderView.frame.size.height;
    [self updateHeaderView];
    [self updateTeamView];
    [self updateDetailsView];
    [self setBackgroundColor:[UIColor clearColor]];
}
-(void)setMatch:(Match *)match
{
    _match = match;
    width =  self.frame.size.width;
    height =  self.frame.size.height;
    [self updateUI];
}
-(void)setFinalMatch
{
    FINAL = YES;
    [HeaderView setFrame:CGRectMake(2, 2, width-4, height/5)];
    [scoreLabel setHighlighted:YES];
    [endLabel setHidden:YES];
    [matchIdLabel setFrame:CGRectMake(5, 2, HeaderView.frame.size.width-5, HeaderView.frame.size.height-4)];
    
    
    
    
    [TeamView setFrame:CGRectMake(2,HeaderView.frame.size.height + 4, width-4, height/2)];
    [team1HammerImageView setHidden:YES];
    [team2HammerImageView setHidden:YES];
    [team1Label setFrame:CGRectMake(10, 5, TeamView.frame.size.width/2, TeamView.frame.size.height/2 - 5)];
    [team2Label setFrame:CGRectMake(10, TeamView.frame.size.height/2 + 5, TeamView.frame.size.width/2, TeamView.frame.size.height/2 - 5)];
    
    
    [DeatilsView setFrame:CGRectMake(2,height -height/5, width-4, height/5)];
}
@end
