//
//  Match.h
//  Bonspiel
//
//  Created by Priya on 12/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Team.h"
#import "Round.h"
//@class Round;
@interface Match : NSObject
@property (nonatomic,strong) Round *round;
@property (nonatomic) NSInteger current_round;
@property (nonatomic) NSInteger sheet;
@property (nonatomic) NSInteger match_id;
@property (nonatomic,strong) NSString *loser_position;
@property (nonatomic) NSInteger winner_round;
@property (nonatomic) NSInteger loser_round;
@property (nonatomic,strong) Team *team1;
@property (nonatomic,strong) Team *team2;
@property (nonatomic) NSInteger winner_position;
@property (nonatomic) BOOL close;
@property (nonatomic,strong) NSString *drawTime;
@property (nonatomic,assign) BOOL isStarted;
-(instancetype) initWithDictionary:(NSDictionary *) dictionary;
-(NSDictionary *)toDict;
@end
