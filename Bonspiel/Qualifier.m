//
//  Qualifier.m
//  Bonspiel
//
//  Created by Priya on 13/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "Qualifier.h"

@implementation Qualifier
@synthesize match1;
@synthesize match2;
@synthesize resultMatch;
-(void)print
{
    NSLog(@"Match 1: %@",match1);
    NSLog(@"Match 2: %@",match2);
    NSLog(@"Result Match: %@",match1);
}
@end
