//
//  BracketsCollectionViewCell.h
//  Bonspiel
//
//  Created by Priya on 12/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Round.h"
@interface BracketsCollectionViewCell : UICollectionViewCell<UITableViewDelegate,UITableViewDataSource>


@property (nonatomic) BOOL isLastRound;
@property (nonatomic,strong) Round *round;
@property (nonatomic,strong) Round *nextRound;
@property (nonatomic,strong) NSMutableSet *cellRefsSet;
@property (nonatomic,strong) UITableView *roundTableView;
-(void)setRoundIndex:(NSInteger) index;
-(void)setCollapsed:(BOOL) collapsed;
-(void) reloadRoundTableView;
-(void) clearCell;
-(UITableView*) getRoundTableView;
-(void) scrollToFirstCell;
@end
