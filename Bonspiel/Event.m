//
//  Event.m
//  Bonspiel
//
//  Created by Priya on 12/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "Event.h"
#import "NSDictionary+json.h"
@implementation Event
@synthesize bonspiel;
@synthesize  bracket;
@synthesize  number;
@synthesize  name;
@synthesize isStarted;
-(instancetype) initWithDictionary:(NSDictionary *) dictionary
{
    self = [super init];
    if (self)
    {
        self.bracket = [[Bracket alloc]initWithDictionary:[dictionary objectForKey:@"brackets"]];
        self.number = [[dictionary objectForKey:@"number"] integerValue] ;
        self.name = [dictionary objectForKey:@"name"];
        self.bracket.event = self;
        self.isStarted = self.bonspiel.isStarted;
    }
    return self;
}
- (NSComparisonResult)compare:(Event *)event;
{
    if (self.number > event.number)
        return NSOrderedAscending;
    else
        return NSOrderedDescending;
}
-(NSDictionary *)toDict
{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[self.bracket toDict] forKey:@"brackets"];
    [dict setObject: [NSNumber numberWithInteger:self.number] forKey:@"number"];
    [dict setObject:self.name forKey:@"name"];
  //  [dict setObject:[self.bonspiel toDict]forKey:@"bonspiel"];
    return dict;
//    return    @{
//                @"brackets":[[self.bracket toDict] ToString],
//                @"number": [NSNumber numberWithInteger:self.number],
//                @"name":self.name,
//                    };
}
@end
