//
//  SearchResultsViewController.m
//  Bonspiel
//
//  Created by Priya on 08/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "SearchResultsViewController.h"
#import "BSAPIService.h"
#import "TournamentViewController.h"
@interface SearchResultsViewController ()
{

    NSDictionary *selectedTournament;
    IBOutlet UITableView *resultTable;

}
@end

@implementation SearchResultsViewController
@synthesize searchResultArray;
-(void) loadView
{
    [super loadView];
    //    UINavigationBar *navBar = [[UINavigationBar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 66)];
//    [self.view addSubview:navBar];
    NSLog(@"");
    [resultTable registerClass:[UITableViewCell class] forCellReuseIdentifier:@"resultCell"];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  //  self.keyword =@"Adsf";

    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed: @"Backcolor.png"]
                   forBarMetrics:UIBarMetricsDefault];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)backButtonClicked:(id)sender
{

    [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchResultArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"resultCell" forIndexPath:indexPath];
    NSDictionary *dict = [self.searchResultArray objectAtIndex:indexPath.row];
    [cell.textLabel setText:[dict objectForKey:@"name"]];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    TournamentViewController *TVC = ;
    selectedTournament = [self.searchResultArray objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"launchTournament" sender:self];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"launchTournament"])
    {
        TournamentViewController *tvc =[segue destinationViewController];
        tvc.tournament = selectedTournament;
    }
}

@end
