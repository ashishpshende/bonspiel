//
//  TournamentTableViewCell.m
//  Bonspiel
//
//  Created by Priya on 08/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "TournamentTableViewCell.h"

@implementation TournamentTableViewCell
@synthesize tournamentAddress;
@synthesize tournamentImageView;
@synthesize tournamentTitle;

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setTournament:(Tournament *)tournament
{
    [tournamentTitle setText:tournament.title];
    [tournamentAddress setText:tournament.address];
    NSURL* imageUrl = [NSURL URLWithString:tournament.image    ];
    NSLog(@"imageUrl %@",imageUrl);
    NSURLRequest* request = [NSURLRequest requestWithURL:imageUrl cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse * response,
                                               NSData * data,
                                               NSError * error) {
                               
                               if (!error && [tournament.image length]>0)
                               {
                                   tournamentImageView.image = [[UIImage alloc] initWithData:data];
                               } else
                               {
                                   //do nothing
                               }
                           }];
    
}

@end
