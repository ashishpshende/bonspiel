//
//  Team.h
//  Bonspiel
//
//  Created by Priya on 12/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Team : NSObject
@property (nonatomic,strong) NSString *skip;
@property (nonatomic) NSInteger seed;
@property (nonatomic,strong) NSString *end;
@property (nonatomic) NSInteger endScore;
@property (nonatomic) NSInteger club;
@property (nonatomic) NSInteger score;
@property (nonatomic) NSInteger hammer;

-(instancetype) initWithDictionary:(NSDictionary *) dictionary;
-(NSDictionary*) toDict;
@end
