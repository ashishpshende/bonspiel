//
//  AXConnetionNotifier.m
//  Axonator
//
//  Created by Ashish on 10/01/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import "AXConnetionNotifier.h"
#import "AFNetworking.h"
#import "Reachability.h"

@implementation AXConnetionNotifier
{
    NSMutableArray *observerArray;
    
}
@synthesize online;
static AXConnetionNotifier *notifier;

+ (AXConnetionNotifier *) sharedNotifier
{
    if(!notifier)
        notifier = [[AXConnetionNotifier alloc] init];
    return notifier;
}



-(id) init
{
    self = [super init];
    if (self)
    {
        observerArray = [[NSMutableArray alloc] init];
        
      
        [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
 
            NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
           
            switch (status)
            {
                    case AFNetworkReachabilityStatusReachableViaWWAN:
                    case AFNetworkReachabilityStatusReachableViaWiFi:
                {
                    [self notifyAllOnline];
                   
                }
                           //[self checkServer];
                    break;
                    case AFNetworkReachabilityStatusNotReachable:
                    
                {   [self notifyAllOffline];
                 
                }
                    default:
                    {
                        [self notifyAllOffline];
                       
                    }
                    break;
            }
        }];
        NSLog(@"%@ Started",self.description);
    }
    return self;
}

-(void) addObserver:( id <AXConnetionObserver> )observer
{
    [observerArray addObject:observer];
}

-(void) notifyAllOnline
{
    NSLog(@"ONLINE MODE");

    for (id <AXConnetionObserver> observer in observerArray)
    {
        [observer notifyOnline];
    }
    self.online = YES;
}
-(void) notifyAllOffline
{
    
    NSLog(@"OFFLINE MODE");

    for (id <AXConnetionObserver> observer in observerArray)
    {
        [observer notifyOffline];
    }
    self.online = NO;
}

@end
