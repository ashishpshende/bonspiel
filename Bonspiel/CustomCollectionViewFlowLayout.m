//
//  CustomCollectionViewFlowLayout.m
//  Bonspiel
//
//  Created by 𝓙𝓐𝓨𝓔𝓢𝓗 on 12/02/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "CustomCollectionViewFlowLayout.h"

@implementation CustomCollectionViewFlowLayout
- (nullable UICollectionViewLayoutAttributes *)finalLayoutAttributesForDisappearingItemAtIndexPath:(NSIndexPath *)itemIndexPath{
    UICollectionViewLayoutAttributes *attr=[super finalLayoutAttributesForDisappearingItemAtIndexPath:itemIndexPath];
    attr.transform=CGAffineTransformTranslate(attr.transform, 0, 50);
    attr.alpha=0.0;
    return attr;
}
@end
