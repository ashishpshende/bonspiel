//
//  BracketsCollectionViewCell.m
//  Bonspiel
//
//  Created by Priya on 12/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "BracketsCollectionViewCell.h"
#import "RoundTableViewCell.h"
#import "Match.h"
#import "Qualifier.h"
#import "Constant.h"
@implementation BracketsCollectionViewCell
{
    
    UILabel *roundLabel;
    NSArray *matchesInCurrentRound;
    UILabel *testLabel;
    NSMutableArray *matches;
    long primaryYSpace;
    long secondaryYSpace;
    long currentRoundIndex;
    long factor;
    BOOL isCollapsed;

}
@synthesize roundTableView;
@synthesize cellRefsSet;
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
        roundLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,65,self.contentView.frame.size.width, 30)];
        
        UIColor *roundLabelBgColor=[[UIColor alloc] initWithRed:88/255.0f green:88/255.0f blue:88/255.0f alpha:1];
        roundLabel.textColor=[UIColor whiteColor];
        roundLabel.font=[UIFont boldSystemFontOfSize:16];
        roundLabel.textAlignment=NSTextAlignmentCenter;
        roundLabel.backgroundColor=roundLabelBgColor;
        [self.contentView addSubview:roundLabel];
        
        
        self.roundTableView = [[UITableView alloc]initWithFrame:CGRectMake(1,roundLabel.frame.origin.y+roundLabel.frame.size.height, self.contentView.frame.size.width, self.contentView.frame.size.height-220)];
        [self.contentView addSubview:self.roundTableView];
        [self.roundTableView registerClass:[RoundTableViewCell class] forCellReuseIdentifier:@"RoundTableViewCell"];
        self.roundTableView.dataSource = self;
        self.roundTableView.delegate = self;
        self.roundTableView.allowsSelection = NO;
        self.roundTableView.separatorColor = [UIColor clearColor];
        self.roundTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.roundTableView.showsHorizontalScrollIndicator=false;
        self.roundTableView.showsVerticalScrollIndicator=false;
        
        isCollapsed = NO;
    }
    return self;
}
-(void) scrollToFirstCell
{
    
}
-(void)setRoundIndex:(NSInteger) index;
{
    
    roundLabel.text=[NSString stringWithFormat:@"Round %ld",index+1];
    currentRoundIndex=index;
    factor=pow(2, currentRoundIndex);
    primaryYSpace=factor;//((HEIGHT_BRACKET_CELL*factor)/HEIGHT_BRACKET_CELL);
    secondaryYSpace=2;//factor;

}
-(void) setRound:(Round *)round{
    matchesInCurrentRound=round.matches;
}
-(void) reloadRoundTableView{
    [self.roundTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}
-(void) setIsLastRound:(BOOL)isLastRound
{
    _isLastRound = isLastRound;
}
-(UITableView*) getRoundTableView{
    return self.roundTableView;
}

-(void)setCollapsed:(BOOL) collapsed
{
    bool prev = isCollapsed;
    isCollapsed = collapsed;
    if(prev != isCollapsed)
        [self reloadRoundTableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *sectionHeader = [[UILabel alloc] initWithFrame:CGRectNull];
    ////sectionHeader.backgroundColor = [UIColor yellowColor];
    roundTableView.tableHeaderView = sectionHeader;
    
    return sectionHeader;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10+ currentRoundIndex*HEIGHT_BRACKET_CELL/4;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return matchesInCurrentRound.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RoundTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RoundTableViewCell" forIndexPath:indexPath];
    Match *match= [matchesInCurrentRound objectAtIndex:indexPath.row];
    cell.match = match;// [cell setMatch:match]
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.matchView.type = indexPath.row % 2;
//    
//     float r = (rand()%255)/255.0f;
//     float g = (rand()%255)/255.0f;
//     float b = (rand()%255)/255.0f;
//    
//    cell.backgroundColor = [UIColor colorWithRed:r green:g blue:b alpha:1];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    long y=HEIGHT_BRACKET_CELL;
    
    if(isCollapsed == NO)
    {

        if((indexPath.row+1)%2==0)
        {
            y=secondaryYSpace*HEIGHT_BRACKET_CELL/2+25;
        }else
        {
            y=secondaryYSpace*HEIGHT_BRACKET_CELL/2+0;
        }
    }
    else
    {
       

        if((indexPath.row+1)%2==0)
        {
            y=HEIGHT_BRACKET_CELL/2+25;
        }else{
            y=HEIGHT_BRACKET_CELL/2+0;
        }
    }
    return y;
}
-(void) clearCell
{
    [roundTableView setHidden:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{

    for (BracketsCollectionViewCell * cell in cellRefsSet) {
        if([cell getRoundTableView]!=roundTableView){
               [[cell getRoundTableView] setContentOffset:scrollView.contentOffset];
        }
    }
}
@end
