//
//  UpdateScoreViewController.m
//  Bonspiel
//
//  Created by 𝓙𝓐𝓨𝓔𝓢𝓗 on 16/02/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "UpdateScoreViewController.h"
#import "Team.h"
#import "BSAPIService.h"
#import "MBProgressHUD.h"
#import "BracketsViewController.h"
@interface UpdateScoreViewController ()
{
    UITapGestureRecognizer *hammer1ImageTapped;
    UITapGestureRecognizer *skip1LabelTapped;
    UITapGestureRecognizer *hammer2ImageTapped;
    UITapGestureRecognizer *skip2LabelTapped;
}

@end

@implementation UpdateScoreViewController
@synthesize  scrollView;
@synthesize  team1HammerImageView;
@synthesize  skip1Label;
@synthesize  team2HammerImageView;
@synthesize  skip2Label;
@synthesize  score1ValueLabel;
@synthesize  score1Stepper;
@synthesize  score2ValueLabel;
@synthesize  score2Stepper;
@synthesize  endValueLabel;
@synthesize  endStepper;
@synthesize  markFinalSwitch;
@synthesize  secretWordTextField;
@synthesize  selectedMatch;
@synthesize  selectedTeam;
@synthesize parentVC;

@synthesize updateButton;
@synthesize cancelButton;
- (void)viewDidLoad
{
    [super viewDidLoad];
//    [self setUpperNavigation];
    [self setGestures];
    [self populateData];
    [self registerForKeyboardNotifications];
    self.secretWordTextField.delegate=self;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"secret_word"])
    {
        self.secretWordTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"secret_word"];
    }
    // Do any additional setup after loading the view.
    if(markFinalSwitch.isOn)
    {
        [self removeGestures];
     //   [self.updateButton setEnabled:NO];
        [self.endStepper setEnabled:NO];
        [score1Stepper setEnabled:NO];
        [score2Stepper setEnabled:NO];
         self.endValueLabel.text=@"F";
    }
    else
    {
   //     [self.updateButton setEnabled:YES];
        [self setGestures];
        [self.endStepper setEnabled:YES];
        [score1Stepper setEnabled:YES];
        [score2Stepper setEnabled:YES];
    }
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//-(void) setUpperNavigation
//{
//    UIBarButtonItem *leftCancelBarButton=[[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelScoreUpdateAction)];
//    [self.navigationController.navigationItem  setLeftBarButtonItem:leftCancelBarButton];
//    
//    UIBarButtonItem *rightUpdateBarButton=[[UIBarButtonItem alloc] initWithTitle:@"Update" style:UIBarButtonItemStylePlain target:self action:@selector(updateScoreAction)];
//    [self.navigationController.navigationItem  setRightBarButtonItem:rightUpdateBarButton];
//
//}
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}
-(void) removeGestures
{
    [team1HammerImageView removeGestureRecognizer:hammer1ImageTapped];;
    [skip1Label removeGestureRecognizer:skip1LabelTapped];
    [team2HammerImageView removeGestureRecognizer:hammer2ImageTapped];
    [skip2Label removeGestureRecognizer:skip2LabelTapped];
}
-(void) setGestures
{
    hammer1ImageTapped=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(team1HammerTapped)];
    hammer1ImageTapped.numberOfTapsRequired=1;
    [team1HammerImageView addGestureRecognizer:hammer1ImageTapped];
    
    skip1LabelTapped=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(team1HammerTapped)];
    skip1LabelTapped.numberOfTapsRequired=1;
    [skip1Label addGestureRecognizer:skip1LabelTapped];
    
    hammer2ImageTapped=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(team2HammerTapped)];
    hammer2ImageTapped.numberOfTapsRequired=1;
    [team2HammerImageView addGestureRecognizer:hammer2ImageTapped];
    
    skip2LabelTapped=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(team2HammerTapped)];
    skip2LabelTapped.numberOfTapsRequired=1;
    [skip2Label addGestureRecognizer:skip2LabelTapped];
}

-(void) populateData{
    //Set basic info
    skip1Label.text=selectedMatch.team1.skip;
    skip2Label.text=selectedMatch.team2.skip;
    score1ValueLabel.text=[NSString stringWithFormat:@"%ld",(long)selectedMatch.team1.score];
    score2ValueLabel.text=[NSString stringWithFormat:@"%ld",(long)selectedMatch.team2.score];
    score1Stepper.value=selectedMatch.team1.score;
    score2Stepper.value=selectedMatch.team2.score;
    
    Team *team=nil;
    if(![selectedMatch.team1.end isEqualToString:@""]){
        [self team1HammerTapped];
        endValueLabel.text=selectedMatch.team1.end;
        team2HammerImageView.backgroundColor=[UIColor whiteColor];
        team2HammerImageView.image=nil;
        team=selectedMatch.team1;
        selectedTeam=TEAM1_HAMMER;
    }else if(![selectedMatch.team2.end isEqualToString:@""]){
        [self team2HammerTapped];
        endValueLabel.text=selectedMatch.team2.end;
        team1HammerImageView.backgroundColor=[UIColor whiteColor];
        team1HammerImageView.image=nil;
        team=selectedMatch.team2;
        selectedTeam=TEAM2_HAMMER;
    }else{
        team1HammerImageView.backgroundColor=[UIColor whiteColor];
        team1HammerImageView.image=nil;
        
        team2HammerImageView.backgroundColor=[UIColor whiteColor];
        team2HammerImageView.image=nil;
    }
    
    if(team){
        if([[team.end lowercaseString] isEqualToString:@"f"]){
            endStepper.enabled=NO;
            [markFinalSwitch setOn:YES];
        }else{
            endStepper.enabled=YES;
            [markFinalSwitch setOn:NO];
            if(![team.end isEqualToString:@""]){
                endStepper.value=[team.end integerValue];
            }
        }
        endValueLabel.text=team.end;
     
    }
    

   
}

-(void) team1HammerTapped
{
    if (!markFinalSwitch.isOn)
    {
        team1HammerImageView.image=[[UIImage imageNamed:@"hammer"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        team2HammerImageView.backgroundColor=[UIColor whiteColor];
        team2HammerImageView.image=nil;
        selectedTeam=TEAM1_HAMMER;
    }
}

-(void) team2HammerTapped
{
    if (!markFinalSwitch.isOn)
    {
    team2HammerImageView.image=[[UIImage imageNamed:@"hammer"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    team1HammerImageView.backgroundColor=[UIColor whiteColor];
    team1HammerImageView.image=nil;
    selectedTeam=TEAM2_HAMMER;
    }
}


- (IBAction)score1StepperValueChangedAction:(id)sender {
    self.score1ValueLabel.text=[NSString stringWithFormat:@"%.0f",score1Stepper.value];
}

- (IBAction)score2StepperValueChangedAction:(id)sender {
      self.score2ValueLabel.text=[NSString stringWithFormat:@"%.0f",score2Stepper.value];
}

- (IBAction)endStepperValueChangedAction:(id)sender
{
    self.endValueLabel.text=[NSString stringWithFormat:@"%.0f",endStepper.value];
}

- (IBAction)finalValueChangedAction:(id)sender
{
    UISwitch *finalSwitch=(UISwitch*)sender;
    if(finalSwitch.isOn)
    {
        [self removeGestures];
        [self.endStepper setEnabled:NO];
        [score1Stepper setEnabled:NO];
        [score2Stepper setEnabled:NO];
        self.endValueLabel.text=@"F";
    }
    else
    {
        endStepper.value = 1;
        self.endValueLabel.text=[NSString stringWithFormat:@"%.0f",endStepper.value];
        [self setGestures];
        [self.endStepper setEnabled:YES];
        [score1Stepper setEnabled:YES];
        [score2Stepper setEnabled:YES];
    }
}

- (IBAction)cancelScoreUpdateAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)updateScoreAction:(id)sender
{
    selectedMatch.team1.score=[score1ValueLabel.text integerValue];
    selectedMatch.team2.score=[score2ValueLabel.text integerValue];
    Team *team=nil;
    
    if(selectedTeam==1)
    {
        selectedMatch.team2.end=@"";
        team=selectedMatch.team1;
        selectedTeam=TEAM1_HAMMER;
    }else if(selectedTeam==2)
    {
        selectedMatch.team1.end=@"";
        team=selectedMatch.team2;
        selectedTeam=TEAM2_HAMMER;
    }

    
    if([markFinalSwitch isOn])
    {

        if(selectedTeam==0)
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Validation error" message:@"Please select hammer." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
         team.end=@"F";
        if(selectedMatch.team1.score==selectedMatch.team2.score)
        {
            if (!(selectedMatch.team1.score==0 && selectedMatch.team2.score)) {
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Validation error" message:@"Score can not be same in final end." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                return;
            }
        }
    }else{
        if([endValueLabel.text integerValue]>0){
            if(selectedTeam==0){
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Validation error" message:@"Please select hammer." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                return;
            }
            team.end=endValueLabel.text;
        }
    }
    
    if(secretWordTextField.text==nil || [secretWordTextField.text isEqualToString:@""])
    {
        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Validation error" message:@"Secret word required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        alert.tag = 1;
        [alert show];
        return;
    }
    
    
    Round *round =  selectedMatch.round;
    Bracket *bracket = round.bracket;
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[bracket.event toDict] forKey:@"event"];
    [dict setObject:self.secretWordTextField.text forKey:@"secret_word"];
    [dict setObject:@"app" forKey:@"type"];
    Event *event = (Event*)bracket.event;
    [dict setObject:[NSString stringWithFormat:@"%ld",(long)event.bonspiel.bonspielid] forKey:@"bonspiel_id"];
   // [[NSNotificationCenter defaultCenter] postNotificationName:@"recordUpdated" object:self userInfo:dict];
    [self Updaterecord:dict];
   
    
}
-(void)Updaterecord:(NSDictionary *) dictionary
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"BonSpiel Alert" message:@"" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    alert.title = @"BonSpiel Alert";
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[BSAPIService sharedAPIService] updateBonspielEventsWithParam:dictionary success:^(id responseObject)
     {
        [[NSUserDefaults standardUserDefaults] setObject:self.secretWordTextField.text forKey:@"secret_word"];
         BracketsViewController *bvc = (BracketsViewController*) self.parentVC;
         [bvc recordUpdated:@"Bonspiel Updated..."];
         [self dismissViewControllerAnimated:YES completion:nil];
         [MBProgressHUD hideHUDForView:self.view animated:YES];
     } failure:^(id error)
    {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
            alert.message  = error;
            [alert show];
     } progress:^(double percentage) {
         
     }];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag ==1)
    {
         secretWordTextField.text = @"";
    }
   
}
-(BOOL) validate
{
    return NO;
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, secretWordTextField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, secretWordTextField.frame.origin.y-kbSize.height);
        [scrollView setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [secretWordTextField resignFirstResponder];
}
@end
