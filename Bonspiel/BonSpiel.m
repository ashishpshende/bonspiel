//
//  BonSpiel.m
//  Bonspiel
//
//  Created by Priya on 12/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "BonSpiel.h"
#import "NSArray+json.h"
#import <UIKit/UIKit.h>
#import "Event.h"
@implementation BonSpiel
{

}
@synthesize  address;
@synthesize  email;
@synthesize  game_guarantee;
@synthesize  host_club;
@synthesize  bonspielid;
@synthesize  name;
@synthesize  number_of_events;
@synthesize  number_of_sheets;
@synthesize  number_of_teams;
@synthesize  phone;
@synthesize  photo;
@synthesize  rules;
@synthesize isStarted;
-(instancetype) initWithDictionary:(NSDictionary *) dictionary
{
    self = [super init];
    if (self)
    {
        self.address = [dictionary objectForKey:@"address"];
        self.email = [dictionary objectForKey:@"email"];
        self.host_club = [dictionary objectForKey:@"host_club"];
        self.name = [dictionary objectForKey:@"name"];
        self.phone = [dictionary objectForKey:@"phone"];
        self.photo = [dictionary objectForKey:@"photo"];
        self.rules = [dictionary objectForKey:@"rules"];
        
        self.number_of_events = [[dictionary objectForKey:@"number_of_events"] integerValue];
        self.number_of_sheets = [[dictionary objectForKey:@"number_of_sheets"]integerValue] ;
        self.number_of_teams = [[dictionary objectForKey:@"number_of_teams"] integerValue] ;
        self.game_guarantee = [[dictionary objectForKey:@"game_guarantee"]integerValue] ;
        self.bonspielid = [[dictionary objectForKey:@"id"] integerValue];
        
        self.isStarted = [[dictionary objectForKey:@"is_started"] boolValue];
        
        [[NSUserDefaults standardUserDefaults] setBool:self.isStarted forKey:@"is_started"];
    }
    return self;
}
-(NSDictionary *) toDict
{
    NSMutableArray *eventDictArray = [NSMutableArray array];
    for (Event *event in self.events)
    {
        [eventDictArray addObject:[event toDict]];
        event.isStarted = self.isStarted ;   //Flag isStarted assigned to each event.
        event.bonspiel = self;
        
    }

    
    return   @{
               @"number_of_events":[NSNumber numberWithInteger:self.number_of_events],
               @"number_of_sheets":[NSNumber numberWithInteger:number_of_sheets],
               @"number_of_teams":[NSNumber numberWithInteger:number_of_teams],
               @"game_guarantee":[NSNumber numberWithInteger:game_guarantee],
               @"bonspiel_id":[NSString stringWithFormat:@"%ld",(long)self.bonspielid],
               @"events": [eventDictArray objectArrayToString],
               @"bonspiel_name":self.name
               };
    
}
@end
