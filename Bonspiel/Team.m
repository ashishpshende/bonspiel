//
//  Team.m
//  Bonspiel
//
//  Created by Priya on 12/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "Team.h"

@implementation Team
@synthesize skip;
@synthesize seed;
@synthesize end;
@synthesize club;
@synthesize score;
@synthesize hammer;
@synthesize endScore;

-(instancetype) initWithDictionary:(NSDictionary *) dictionary
{
    self = [super init];
    if (self)
    {
        self.skip = [dictionary objectForKey:@"skip"]; if(!self.skip) self.skip =@"";
        self.seed = [[dictionary objectForKey:@"seed"] integerValue];
        self.end = [dictionary objectForKey:@"end"];if(!self.end) self.end =@"";
        if (![self.end isEqualToString:@"f"] && ![self.end isEqualToString:@"f"] && ![self.end isEqualToString:@""] )
        {
            self.endScore = [self.end integerValue];
        }
        self.club = [[dictionary objectForKey:@"club"] integerValue];
        self.score = [[dictionary objectForKey:@"score"] integerValue];
        self.hammer = [[dictionary objectForKey:@"hammer"] integerValue];
    }
    return self;
}
-(NSDictionary *) toDict
{
    return @{
             @"skip" : self.skip,
             @"seed" : [NSNumber numberWithInteger:self.seed],
             @"end" : self.end,
             @"club":[NSNumber numberWithInteger:self.club],
             @"score":[NSNumber numberWithInteger:self.score],
             @"hammer" : [NSNumber numberWithInteger:hammer]
                 };
}
@end
