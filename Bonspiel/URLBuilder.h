//
//  URLBuilder.h
//  Bonspiel
//
//  Created by Priya on 08/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface URLBuilder : NSObject
+(NSString *)buildURL:(NSString *) apiName;
+(NSURL *) fileURL:(NSString *)filePath;
@end
