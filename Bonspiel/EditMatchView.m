//
//  EditMatchView.m
//  Bonspiel
//
//  Created by Priya on 13/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "EditMatchView.h"
#import "BSAPIService.h"
#import "Bracket.h"
#import "Event.h"
#import "NSDictionary+json.h"
@implementation EditMatchView
{
    __block  IBOutlet  UIImageView *team1hammerImageView;
    __block  IBOutlet  UIImageView *team2hammerImageView;
}
@synthesize  matchIdLabel;

@synthesize  team1Label;
@synthesize team1Switch;

@synthesize  team2Label;
@synthesize team2Switch;

@synthesize  team1ScoreLabel;
@synthesize  team1ScorePlusButton;
@synthesize  team1ScoreMinusButton;

@synthesize  team2ScoreLabel;
@synthesize  team2ScorePlusButton;
@synthesize  team2ScoreMinusButton;

@synthesize  sheetLabel;
@synthesize  drawTimeLabel;
@synthesize  loserPositionLabel;

@synthesize  endLabel;
@synthesize endSwitch;
@synthesize  endPlusButton;
@synthesize  endMinusButton;

@synthesize  secretKeyTextField;

@synthesize team1hammerImageView;
@synthesize team2hammerImageView;
-(void) setUI
{
    [self.layer setBorderWidth:2];
    [self.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    self.layer.masksToBounds = NO;
    self.layer.shadowOffset = CGSizeMake(-15, 20);
    self.layer.shadowRadius = 5;
    self.layer.shadowOpacity = 0.5;

    __weak typeof(team1hammerImageView) weakTeam1hammerImageView = team1hammerImageView;

    __weak typeof(team2hammerImageView) weakTeam2hammerImageView = team2hammerImageView;

     __weak typeof(team1Switch) weakteam1Switch = team1Switch;
    __weak typeof(team2Switch) weakteam2Switch = team2Switch;
    [team1Switch setBlocks_switchOn:^{
                [weakTeam1hammerImageView setHidden:NO];
                [weakTeam2hammerImageView setHidden:YES];
                [weakteam2Switch setOn:NO];
    } switchOff:^{
        [weakTeam2hammerImageView setHidden:NO];
        [weakTeam1hammerImageView setHidden:YES];
        [weakteam2Switch setOn:YES];
    }];
    
    [team2Switch setBlocks_switchOn:^{
        [weakteam1Switch setOn:NO];
        [weakTeam2hammerImageView setHidden:NO];
        [weakTeam1hammerImageView setHidden:YES];
    } switchOff:^{
         [weakteam1Switch setOn:YES];
        [weakTeam1hammerImageView setHidden:NO];
        [weakTeam2hammerImageView setHidden:YES];
    }];
    
    
    __weak typeof(endLabel) weakendLabel = endLabel;
    __weak typeof(_match) weak_match = _match;
    __weak typeof(endPlusButton) weakendPlusButton = endPlusButton;
    __weak typeof(endMinusButton) weakendMinusButton = endMinusButton;
    [endSwitch setBlocks_switchOn:^{
        [weakendLabel setText:@"F"];
        [weakendPlusButton setEnabled:NO];
        [weakendPlusButton setBackgroundColor:[UIColor lightGrayColor]];
        [weakendMinusButton setEnabled:NO];
        [weakendMinusButton setBackgroundColor:[UIColor lightGrayColor]];

    } switchOff:^{
        [weakendPlusButton setEnabled:YES];
        [weakendPlusButton setBackgroundColor:[UIColor blueColor]];
        [weakendMinusButton setEnabled:YES];
        [weakendMinusButton setBackgroundColor:[UIColor blueColor]];

        if (weak_match.team1.end)
        {
            [weakendLabel setText:[NSString stringWithFormat:@"%ld",(long)weak_match.team1.endScore]];
        }
        else
        {
             [weakendLabel setText:[NSString stringWithFormat:@"%ld",(long)weak_match.team2.endScore]];
        }
        if (weak_match.team2.end)
        {
            [weakendLabel setText:[NSString stringWithFormat:@"%ld",(long)weak_match.team2.endScore]];
        }
        else
        {	
            [weakendLabel setText:[NSString stringWithFormat:@"%ld",(long)weak_match.team1.endScore]];
        }
    }];
    [secretKeyTextField addTarget:self action:@selector(textFieldClicked:) forControlEvents:UIControlEventEditingDidBegin];
        [secretKeyTextField addTarget:self action:@selector(textFieldClicked:) forControlEvents:UIControlEventEditingChanged];
}
-(void)textFieldClicked:(id)sender
{
    
}
-(void) setMatch:(Match *)match
{
    _match=match;
    [self.matchIdLabel setText:[NSString stringWithFormat:@"%ld",(long)_match.match_id]];
    [self.team1Label setText:_match.team1.skip];
    [self.team2Label setText:_match.team2.skip];
    [self.team1ScoreLabel setText:[NSString stringWithFormat:@"%ld",(long)_match.team1.score]];
    [self.team2ScoreLabel setText:[NSString stringWithFormat:@"%ld",(long)_match.team2.score]];
    [sheetLabel setText:[NSString stringWithFormat:@"Sheet: %ld",(long)_match.sheet]];
    [drawTimeLabel setText:_match.drawTime];
    [loserPositionLabel setText:_match.loser_position];
    
    [team1ScoreMinusButton setFrame:CGRectMake(team1ScoreMinusButton.frame.origin.x, team1ScoreMinusButton.frame.origin.y, 25, 25)];
    [team1ScoreMinusButton.layer setCornerRadius:12];
    [team1ScoreMinusButton clipsToBounds];
    
    [team1ScorePlusButton setFrame:CGRectMake(team1ScorePlusButton.frame.origin.x, team1ScorePlusButton.frame.origin.y, 25, 25)];
    [team1ScorePlusButton.layer setCornerRadius:12];
    [team1ScorePlusButton clipsToBounds];

    [team2ScoreMinusButton setFrame:CGRectMake(team2ScoreMinusButton.frame.origin.x, team2ScoreMinusButton.frame.origin.y, 25, 25)];
    [team2ScoreMinusButton.layer setCornerRadius:12];
    [team2ScoreMinusButton clipsToBounds];

    [team2ScorePlusButton setFrame:CGRectMake(team2ScorePlusButton.frame.origin.x, team2ScorePlusButton.frame.origin.y, 25, 25)];
    [team2ScorePlusButton.layer setCornerRadius:12];
    [team2ScorePlusButton clipsToBounds];
 
    [endMinusButton setFrame:CGRectMake(endMinusButton.frame.origin.x, endMinusButton.frame.origin.y, 25, 25)];
    [endMinusButton.layer setCornerRadius:12];
    [endMinusButton clipsToBounds];
    
    
    [endPlusButton setFrame:CGRectMake(endPlusButton.frame.origin.x, endPlusButton.frame.origin.y, 25, 25)];
    [endPlusButton.layer setCornerRadius:12];
    [endPlusButton clipsToBounds];
}
- (IBAction)team1ScorePlusButtonTapped:(UIButton *)sender;
{
    _match.team1.score ++;
    [self.team1ScoreLabel setText:[NSString stringWithFormat:@"%ld",(long)_match.team1.score]];
}

- (IBAction)team1ScoreMinusButtonTapped:(UIButton *)sender;
{
    if (_match.team1.score !=0)
    {
        _match.team1.score --;
        [self.team1ScoreLabel setText:[NSString stringWithFormat:@"%ld",(long)_match.team1.score]];

    }
   }
- (IBAction)team2ScorePlusButtonTapped:(UIButton *)sender
{
       _match.team2.score ++;
     [self.team2ScoreLabel setText:[NSString stringWithFormat:@"%ld",(long)_match.team2.score]];
}
- (IBAction)team2ScoreMinusButtonTapped:(UIButton *)sender;
{
    if (_match.team2.score !=0)
    {
        _match.team2.score --;
         [self.team2ScoreLabel setText:[NSString stringWithFormat:@"%ld",(long)_match.team2.score]];
    }
}
- (IBAction)endPlusButtonTapped:(UIButton *)sender
{
    if (!endSwitch.isOn)
    {
        _match.team2.endScore ++;
        [self.endLabel setText:[NSString stringWithFormat:@"%ld",(long)_match.team2.endScore]];
    }
//    if (_match.team2.end)
//    {
//        _match.team2.endScore ++;
//        [self.endLabel setText:[NSString stringWithFormat:@"%ld",(long)_match.team2.endScore]];
//    }
//    
//    if (_match.team1.end)
//    {
//        _match.team1.endScore ++;
//        [self.endLabel setText:[NSString stringWithFormat:@"%ld",(long)_match.team1.endScore]];
//    }

}

- (IBAction)endMinusButtonTapped:(UIButton *)sender
{
//    if (_match.team2.end)
//    {
//        if (_match.team2.endScore !=0)
//        {
//            _match.team2.endScore --;
//            [self.endLabel setText:[NSString stringWithFormat:@"%ld",(long)_match.team2.endScore]];
//        
//        }
//    }
//    if (_match.team1.end)
//    {
//        if (_match.team1.endScore !=0)
//        {
//            _match.team1.endScore --;
//            [self.endLabel setText:[NSString stringWithFormat:@"%ld",(long)_match.team1.endScore]];
//            
//        }
//    }
    if (!endSwitch.isOn)
    {
        _match.team2.endScore --;
        [self.endLabel setText:[NSString stringWithFormat:@"%ld",(long)_match.team2.endScore]];
    }
}

- (IBAction)cancelButtonTapped:(UIButton *)sender
{
    [self setHidden:YES];
     [self endEditing:YES];
}
-(BOOL) validate
{
    if ([self.secretKeyTextField.text isEqualToString:@""])
    {
        return NO;
    }
    if(!([self.team1Switch isOn] || [self.team2Switch isOn])){
        return NO;
    }
    return YES;
}
- (IBAction)submitButtonTapped:(UIButton *)sender
{
     [self endEditing:YES];
    if ([self validate])
    {
        //check end
        if([endSwitch isOn]){
            if([team1Switch isOn]){
                [_match.team1 setEnd:@"F"];
            }else if([team2Switch isOn]){
                [_match.team2 setEnd:@"F"];
            }
        }
        
        [self setHidden:YES];
        NSLog(@"%@",_match.round);
        Round *round =  _match.round;
        Bracket *bracket = round.bracket;
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:[bracket.event toDict] forKey:@"event"];
        [dict setObject:self.secretKeyTextField.text forKey:@"secret_word"];
        [dict setObject:@"app" forKey:@"type"];
        Event *event = (Event*)bracket.event;
        [dict setObject:[NSString stringWithFormat:@"%ld",(long)event.bonspiel.bonspielid] forKey:@"bonspiel_id"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"recordUpdated" object:self userInfo:dict];
        secretKeyTextField.text = @"";
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert." message:@"Please Enter Secret Keyword." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];

    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
     [self endEditing:YES];// this will do the trick
     [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height)];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y-150, self.frame.size.width, self.frame.size.height)];
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Alert Message" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    [alert show];
    
    return YES;
}
@end
