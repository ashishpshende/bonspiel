//
//  BSAPIService.m
//  Bonspiel
//
//  Created by Priya on 10/12/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import "BSAPIService.h"
#import "OfflineNetworkService.h"
#import "OnlineNetworkService.h"
#import "AXConnetionNotifier.h"
@implementation BSAPIService
{
    NetworkService *networkService;
    OfflineNetworkService *offlineNetworkService;
    OnlineNetworkService *onlineNetworkService;
}
static BSAPIService *APIService;

+(instancetype)sharedAPIService
{
    if (!APIService)
    { 
         APIService = [[BSAPIService alloc]init];
        
    }
    return APIService;
}

-(instancetype)init
{
    self = [super init];
    if (self)
    {
        [[AXConnetionNotifier sharedNotifier] addObserver:self];
        offlineNetworkService = [[OfflineNetworkService alloc]init];
        onlineNetworkService = [[OnlineNetworkService alloc]init];
    }
    return self;
}
-(void) notifyOnline
{
    networkService = onlineNetworkService;
}
-(void) notifyOffline
{
    networkService = offlineNetworkService;
}

-(void) authenticate:(NSArray *)userDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage;
{
    [networkService authenticate:userDict success:success failure:failure progress:percentage];
}
-(void) searchWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage;
{
    [networkService searchWithParam:paramDict success:success failure:failure progress:percentage];
}
-(void) loadTournamentWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage;
{
    [networkService loadTournamentWithParam:paramDict success:success failure:failure progress:percentage];
}
-(void) loadBonspielEventsWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage
{
    [networkService loadBonspielEventsWithParam:paramDict success:success failure:failure progress:percentage];
}
-(void) updateBonspielEventsWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage
{
    [networkService updateBonspielEventsWithParam:paramDict success:success failure:failure progress:percentage];
}

@end
