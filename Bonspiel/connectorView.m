//
//  connectorView.m
//  Bonspiel
//
//  Created by Jayesh Kitukale on 26/03/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "connectorView.h"

@implementation connectorView
@synthesize type;

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(context, [[UIColor alloc] initWithRed:178/255.0f green:177/255.0f blue:177/255.0f alpha:1].CGColor);
    
    CGContextSetLineWidth(context, 1.0);

    CGContextMoveToPoint(context, 0.0, self.frame.size.height/2);

    if (self.type == 0) {
        CGContextAddLineToPoint(context, self.frame.size.width/2, self.frame.size.height/2);
        CGContextAddLineToPoint(context,  self.frame.size.width/2, self.frame.size.height);
        CGContextAddLineToPoint(context,  self.frame.size.width, self.frame.size.height);
    }
    else if (self.type == 1) {
        CGContextAddLineToPoint(context, self.frame.size.width/2, self.frame.size.height/2);
        CGContextAddLineToPoint(context,  self.frame.size.width/2, 0);
        CGContextAddLineToPoint(context,  self.frame.size.width, 0);
    }
    
    CGContextDrawPath(context, kCGPathStroke);
    
}

@end
