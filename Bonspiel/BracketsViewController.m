//
//  BracketsViewController.m
//  Bonspiel
//
//  Created by Priya on 12/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "BracketsViewController.h"
#import "BSAPIService.h"
#import "Event.h"
#import "Bracket.h"
#import "BracketsCollectionViewCell.h"
#import "CustomCollectionViewFlowLayout.h"
#import "UpdateScoreViewController.h"
#import "MBProgressHUD.h"
#import "iToast.h"
@interface BracketsViewController ()
{
    Event *selectedEvent;
    __weak IBOutlet UILabel *eventNumberLabel;
    __weak IBOutlet UITabBar *evenTabbar;
    __weak IBOutlet UILabel *roundLabel;
    __weak IBOutlet UILabel *nextRoundLabel;
    UICollectionView *bracketCollectionView;
    ScrollDirection scrollDirection;
    CGRect oldCellFrame;
    CGAffineTransform oldCellTransform;
    CGRect oldCollectionFrame;
    NSMutableSet *cellRefsSet;
    long pageControl_currentPage;
    long current_round;
    NSInteger lastSelectedEventIndex;
}
@end

@implementation BracketsViewController
@synthesize loader;
@synthesize bonspiel;
@synthesize noEventLabel;
@synthesize editMatchView;
@synthesize lastContentOffset;
-(void) loadView
{
    [super loadView];
  
   
}

-(void) recordUpdated:(NSString *)message
{
    iToast * objiTost = [iToast makeText:message];
    [objiTost setFontSize:14];
    [objiTost setDuration:iToastDurationNormal];
    [objiTost setGravity:iToastGravityBottom];
    [objiTost show];

    [self prepareBrackets];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [NSTimer scheduledTimerWithTimeInterval: 60.0 target: self
                                                      selector: @selector(reloadData:) userInfo: nil repeats: YES];

    current_round = 0;
    cellRefsSet=[[NSMutableSet alloc] init];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
     [evenTabbar setFrame:CGRectMake(0, self.view.frame.size.height-evenTabbar.frame.size.height, self.view.frame.size.width, evenTabbar.frame.size.height)];

    UICollectionViewFlowLayout *flowLayout = [[CustomCollectionViewFlowLayout alloc] init] ;
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    bracketCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, roundLabel.frame.origin.y + roundLabel.frame.size.height+1, self.view.frame.size.width, self.view.frame.size.height+60) collectionViewLayout:flowLayout];
    [bracketCollectionView setCollectionViewLayout:flowLayout animated:YES];
    [bracketCollectionView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];

    
   
    [bracketCollectionView setCollectionViewLayout:flowLayout animated:YES];
    [self.view addSubview:bracketCollectionView];
    [bracketCollectionView setBackgroundColor:[UIColor whiteColor]];
    bracketCollectionView.dataSource =self;
    bracketCollectionView.delegate = self;
    bracketCollectionView.pagingEnabled = NO;
    bracketCollectionView.scrollEnabled = NO;
    
    
    //LEFT GEST
    UISwipeGestureRecognizer* swipeUpGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeLeftFrom:)];
    swipeUpGestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    
    [bracketCollectionView addGestureRecognizer:swipeUpGestureRecognizer];

    
    //RIGHT GEST
    UISwipeGestureRecognizer* swipeRightGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRightFrom:)];
    swipeRightGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    
    [bracketCollectionView addGestureRecognizer:swipeRightGestureRecognizer];
    [bracketCollectionView registerClass:[BracketsCollectionViewCell class] forCellWithReuseIdentifier:@"BracketsCollectionViewCell"];
    [self prepareBrackets];
    [self.navigationItem setTitle:bonspiel.name];
    [self.navigationItem.leftBarButtonItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
    
    
    // Do any additional setup after loading the view.
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(matchViewClicked:) name:@"matchViewClicked" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recordUpdated:) name:@"recordUpdated" object:nil];
    [self.editMatchView setUI];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed: @"Backcolor.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"refreshButton.png"] style:UIBarButtonItemStylePlain target:self action:@selector(reloadData:)];
    [self.navigationItem setRightBarButtonItem:refreshButton];
    // Do any additional setup after loading the view.
}
-(void) refreshButtonTapped:(id) sender
{
    NSLog(@"%@",[NSDate date]);
}

- (void)handleSwipeLeftFrom:(UIGestureRecognizer*)recognizer {
    
    // Add Guard condition on right side here
    if (current_round==selectedEvent.bracket.rounds.count-1)
    {
        return;
    }
    current_round++;
    @try {
        NSIndexPath* path = [NSIndexPath indexPathForRow:current_round inSection:0];
        [bracketCollectionView scrollToItemAtIndexPath:path atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
        
        
        NSIndexPath* path1 = [NSIndexPath indexPathForRow:current_round inSection:0];
        BracketsCollectionViewCell *cell1 = (BracketsCollectionViewCell*)[bracketCollectionView cellForItemAtIndexPath:path1];
        [cell1 setCollapsed:YES];
        [cell1 scrollToFirstCell];
        
        NSIndexPath* path2 = [NSIndexPath indexPathForRow:current_round+1 inSection:0];
        BracketsCollectionViewCell *cell2 = (BracketsCollectionViewCell*)[bracketCollectionView cellForItemAtIndexPath:path2];
        [cell2 setCollapsed:NO];
    }
    @catch (NSException *exception) {
         NSLog(@"NSException %@",exception);
    }
    @finally {
        NSLog(@"In FInally Block");
    }
    
    
}
- (void)handleSwipeRightFrom:(UIGestureRecognizer*)recognizer {
    
    // Add Guard condition on left side here
    if (current_round==0) {
        return;
    }
    current_round--;
    NSIndexPath* path = [NSIndexPath indexPathForRow:current_round inSection:0];
    [bracketCollectionView scrollToItemAtIndexPath:path atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];

    NSIndexPath* path1 = [NSIndexPath indexPathForRow:current_round inSection:0];
    BracketsCollectionViewCell *cell1 = (BracketsCollectionViewCell*)[bracketCollectionView cellForItemAtIndexPath:path1];
    [cell1 setCollapsed:YES];
    
    
    NSIndexPath* path2 = [NSIndexPath indexPathForRow:current_round+1 inSection:0];
    BracketsCollectionViewCell *cell2 = (BracketsCollectionViewCell*)[bracketCollectionView cellForItemAtIndexPath:path2];
    [cell2 setCollapsed:NO];
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


-(void)matchViewClicked:(NSNotification *) notification
{
    
    
    UIStoryboard *storybord=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UpdateScoreViewController *updateScoreViewController=[storybord instantiateViewControllerWithIdentifier:@"UpdateScore"];
    updateScoreViewController.selectedMatch=[notification.userInfo objectForKey:@"match"];
    updateScoreViewController.parentVC = self;
    [self.navigationController presentViewController:updateScoreViewController animated:YES completion:nil];
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }

    [self.loader setHidden:YES];
}
-(void) reloadData:(NSNotification *) notification
{
    NSLog(@"Refreshing Bonspiel @ %@",[NSDate date]);
    [self.loader setHidden:NO];
    [self prepareBrackets];

}
-(void) prepareBrackets
{
    NSDictionary *dict = @{
                           @"bonspiel_id":[NSNumber numberWithInteger:self.bonspiel.bonspielid]
                           };
    [self .loader startAnimating];
    [[BSAPIService sharedAPIService] loadBonspielEventsWithParam:dict success:^(id responseObject)
    {
        [self .loader stopAnimating];
        self.bonspiel.game_guarantee = [[responseObject objectForKey:@""]integerValue];
        
        NSMutableArray *events = [NSMutableArray array];
        
        for (NSDictionary *dictionary in [responseObject objectForKey:@"events"])
        {
            Event *event = [[Event alloc]initWithDictionary:dictionary];
            event.bonspiel = self.bonspiel;
            [events addObject:event];
        }
        self.bonspiel.events = (NSArray *) events;
        
        [self sortEvents];
        
        [self drawBrackets];
        
        if (events.count !=0)
        {
              [evenTabbar setSelectedItem:[evenTabbar.items objectAtIndex:lastSelectedEventIndex]];
              selectedEvent = [self.bonspiel.events objectAtIndex:lastSelectedEventIndex];
              [noEventLabel setHidden:YES];
              [roundLabel setHidden:NO];
              [eventNumberLabel setHidden:NO];
        }
        else
        {
                [noEventLabel setHidden:NO];
                [roundLabel setHidden:YES];
                [eventNumberLabel setHidden:YES];
        }
        
        [bracketCollectionView reloadData];
    } failure:^(id error)
    {
         [self .loader stopAnimating];
    } progress:^(double percentage) {
        
    }];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = bracketCollectionView.frame.size.width;
    float currentPage = bracketCollectionView.contentOffset.x / pageWidth;
    
    if (0.0f != fmodf(currentPage, 1.0f))
    {
        pageControl_currentPage = currentPage + 1;
    }
    else
    {
        pageControl_currentPage = currentPage;
    }
    
    NSLog(@"Page Number : %ld", (long)pageControl_currentPage);


    NSIndexPath* path = [NSIndexPath indexPathForRow:pageControl_currentPage inSection:0];
    BracketsCollectionViewCell *cell = (BracketsCollectionViewCell*)[bracketCollectionView cellForItemAtIndexPath:path];
    [cell setCollapsed:YES];
    
    
    NSIndexPath* path2 = [NSIndexPath indexPathForRow:pageControl_currentPage+1 inSection:0];
    BracketsCollectionViewCell *cell2 = (BracketsCollectionViewCell*)[bracketCollectionView cellForItemAtIndexPath:path2];
    
    [cell2 setCollapsed:NO];
    
    
}



-(void) sortEvents
{
    NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc]initWithKey:@"number" ascending:YES];
    NSArray * descriptors = [NSArray arrayWithObject:valueDescriptor];
    NSArray *sortedArray=[self.bonspiel.events sortedArrayUsingDescriptors:descriptors];
    self.bonspiel.events = sortedArray;
}

-(void) drawBrackets
{
    [evenTabbar setFrame:CGRectMake(0, self.view.frame.size.height-evenTabbar.frame.size.height, self.view.frame.size.width, evenTabbar.frame.size.height)];
    NSMutableArray *tabs = [NSMutableArray array];
   
    for (Event *event in self.bonspiel.events)
    {
        NSString *title = [NSString stringWithFormat:@"Event %ld",(long)event.number];
        UITabBarItem *tabBarItem = [[UITabBarItem alloc]initWithTitle:title  image:[UIImage imageNamed:@"stone_unselected.png"] tag:[self.bonspiel.events indexOfObject:event ]];
        [tabs addObject:tabBarItem];
    }
    [eventNumberLabel setText:@"Event 1"];// for first time load
    evenTabbar .items = tabs;
}
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item // called when a new view is selected by the user (but not programatically)
{
    [eventNumberLabel setText:item.title];
    [item setImage:[UIImage imageNamed:@"stone_selected.png"]];
    selectedEvent = [self.bonspiel.events objectAtIndex:item.tag];
    lastSelectedEventIndex=item.tag;
    [bracketCollectionView reloadData];
    [bracketCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Collection View Cells
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0f;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0f;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
   return CGSizeMake(collectionView.frame.size.width*4/5, collectionView.frame.size.height);
   // return collectionView.frame.size;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return selectedEvent.bracket.rounds.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BracketsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BracketsCollectionViewCell" forIndexPath:indexPath];
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    [cellRefsSet addObject:cell];
    cell.cellRefsSet=cellRefsSet;
    Round *round = [selectedEvent.bracket.rounds objectAtIndex:indexPath.row];
    [cell setRound:round];
    UITableView *tableView=[cell getRoundTableView];
    [cell setRoundIndex:indexPath.row];
    
    if(indexPath.row == 0)
       [cell setCollapsed:YES];
    [UIView setAnimationsEnabled:NO];
    [tableView beginUpdates];
    [tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
    [tableView endUpdates];
    [UIView setAnimationsEnabled:YES];
    return cell;
}
@end
