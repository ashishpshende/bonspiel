//
//  Switch.h
//  Bonspiel
//
//  Created by Priya on 13/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Switch : UISwitch
-(void) setBlocks_switchOn:(void (^)(void))on switchOff:(void (^)(void))off;
-(instancetype) initWithFrame:(CGRect)frame switchOn:(void (^)(void))on switchOff:(void (^)(void))off;
@end
