//
//  OfflineNetworkService.m
//  Bonspiel
//
//  Created by Priya on 08/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import "OfflineNetworkService.h"

@implementation OfflineNetworkService
-(void) authenticate:(NSArray *)userDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage
{
    failure(@"You seem to be offline.");
}
-(void) searchWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage
{
    failure(@"You  seem to be offline.");
}
-(void) loadTournamentWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage;
{
     failure(@"You seem to be offline.");
}
-(void) loadBonspielEventsWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage
{
     failure(@"You seem to be offline.");   
}
-(void) updateBonspielEventsWithParam:(NSDictionary *)paramDict  success:(void (^)(id responseObject))success  failure:(void (^)( id error))failure progress:(void (^)( double percentage))percentage
{
    
}
@end
