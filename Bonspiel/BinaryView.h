//
//  BinaryView.h
//  Bonspiel
//
//  Created by Priya on 20/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BinaryView : UIView
-(void) updateView;
@end
