//
//  AXConnetionNotifier.h
//  Axonator
//
//  Created by Ashish on 10/01/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AXConnetionObserver.h"

@interface AXConnetionNotifier : NSObject
+ (AXConnetionNotifier *) sharedNotifier;
@property (nonatomic) BOOL online;
-(void) addObserver:( id <AXConnetionObserver> )observer;
-(void) notifyAllOnline;
-(void) notifyAllOffline;
@end
