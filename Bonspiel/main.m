//
//  main.m
//  Bonspiel
//
//  Created by Priya on 10/12/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
