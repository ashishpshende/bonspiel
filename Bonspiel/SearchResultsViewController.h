//
//  SearchResultsViewController.h
//  Bonspiel
//
//  Created by Priya on 08/01/16.
//  Copyright © 2016 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultsViewController : UITableViewController <UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong) NSArray  *searchResultArray;

@end
